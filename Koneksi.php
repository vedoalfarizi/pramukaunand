<?php

/**
 * Created by PhpStorm.
 * User: vedo_alfarizi
 * Date: 23/06/2017
 * Time: 00.05
 */
class Koneksi
{
    public $kon;
    private $host   = 'localhost';
    private $user   = 'root';
    private $pass   = '';
    private $db     = 'pramuka';

    function __construct(){
        $this->kon = new mysqli($this->host, $this->user, $this->pass, $this->db);
    }
}