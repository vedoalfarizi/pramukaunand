<?php
include "Fungsi.php";
$func = new Fungsi();
/**
 * Created by PhpStorm.
 * User: vedo_alfarizi
 * Date: 24/06/2017
 * Time: 15.09
 */
$id = $_GET['berita'];
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Pramuka Unand</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600' rel='stylesheet' type='text/css'>


    <link rel="shortcut icon" href="assets/images/logo-racana.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">

</head>
<body>

<?php include'header.php'; ?>

<div class="container">
    <?php
        $berita = $func->showOneBerita($id)->fetch_assoc();
        echo "
            <div class=\"cnt-title\"><h1 class='text-primary'>".$berita['judul']."</h1></div>
            <div>
                <div class=\"col-5 noborder\">
                    <img src=\"assets/images/news/".$berita['foto']."\" alt=\"\" class=\"img-responsive\">
                </div>
                <div class=\"text-primary text-justify\">
                    ".$berita['isi']."
                </div>
        
            </div>    
        ";
    ?>

</div>
<div class="col-1 noborder">
    <a href="berita.php" class="navbar-link text-primary">Kembali</a>
</div>

<?php include 'footer.php';?>

<script src="assets/js/jquery.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/style.js"></script>

</body>
</html>

