<div id="header-row">
	<div class="container">
        <div class="row" style="text-align: center; color:white;">
            <div class="col-1"><a class="brand" href="#"><img src="assets/images/logo pramuka.png"/ style="max-width: 50px"></a></div>
            <div class="col-9" style="font-family: 'Verdana', Helvetica, Arial, sans-serif; font-size: 200%; line-height: 100%;">RACANA PUTERA PUTERI SWARNADWIPA <br> GUDEP PADANG 06.067-06.068 <br> UNIVERSITAS ANDALAS</div>
            <div class="col-1"><a class="brand" href="#"><img src="assets/images/logo-racana.png" style="max-width: 110px"></a></div>
        </div>
		<div class="row" style="text-align: center">
            <div class="col-3"></div>
			<div class="col-6">
				<div id="navigasi" class="navbar">
					<div class="navbar-default">
						<a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a>
						<div class="nav-collapse collapse navbar-responsive-collapse">
							<ul class="nav">
								<li class="active"><a href="index.php">Beranda</a></li>
								<li><a href="galeri.php">Galeri</a></li>
								<li><a href="fiesta/index.php">Fiesta</a></li>
								<li><a href="about.php">Tentang</a></li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">Anggota <b class="caret"></b></a>
									<ul class="dropdown-menu">
										<li><a href="pengurus.php">Pengurus</a></li>
										<li><a href="anggota.php">Anggota</a></li>
										<li><a href="alumni.php">Alumni</a></li>
									</ul>
								</li>
								<li><a href="berita.php">Berita</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
