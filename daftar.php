<?php
session_start();
/**
 * Created by PhpStorm.
 * User: vedo_alfarizi
 * Date: 23/06/2017
 * Time: 16.00
 */
include 'Fungsi.php';
$func = new Fungsi();
$status = $_GET['status'];
if(!isset($_SESSION['pesan'])){
    $_SESSION['pesan']="";
}
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Pramuka Unand</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600' rel='stylesheet' type='text/css'>


    <link rel="shortcut icon" href="assets/images/logo-racana.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">

</head>
<body>

<?php include'header.php'; ?>

<div class="container">
    <?php
        if($status==1){
            echo "
                <div>
                    <h2 class=\"text-center\">Daftar Menjadi Anggota</h2>
                </div>        
            ";
        }else if($status==2){
            echo "
                <div>
                    <h2 class=\"text-center\">Daftar Menjadi Alumni</h2>
                </div>        
            ";
        }

        if($_SESSION['pesan']!=""){
            echo "
                <div class=\"alert alert-danger text-center\">
                    ".$_SESSION['pesan']."
                </div>
            ";
        }
    ?>

    <div class="form-center">
        <form action="route.php" method="post">

            <div class="input-group">
                <input type="text" class="form-control" placeholder="Nama" name="nama" aria-describedby="basic-addon1">
            </div>

            <div class="input-group">
                <input type="text" class="form-control" placeholder="No Handphone" name="no_hp" aria-describedby="basic-addon1">
            </div>

            <div class="input-group">
                <input type="email" class="form-control" placeholder="Email" name="email" aria-describedby="basic-addon1">
            </div>

            <div class="input-group">
                <select name="jenis_k">
                    <option value="1">Putera</option>
                    <option value="2">Puteri</option>
                </select>
            </div>

            <div class="input-group">
                <textarea placeholder="Alamat" name="alamat" cols="30" rows="3"></textarea>
            </div>

            <input type="hidden" name="status" value="<?php echo "$status" ?>">

            <div class="input-group">
                <input type="submit" class="form-control btn-primary" value="Daftar" name="daftar" aria-describedby="basic-addon1">
            </div>
        </form>
    </div>

</div>

<?php include 'footer.php';?>

<script src="assets/js/jquery.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/style.js"></script>

</body>
</html>


<?php $_SESSION['pesan']=""; ?>