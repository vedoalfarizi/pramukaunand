<?php
session_start();
include 'Fungsi.php';
$func = new Fungsi();
/**
 * Created by PhpStorm.
 * User: vedo_alfarizi
 * Date: 23/06/2017
 * Time: 17.10
 */

//menambahkan anggota dan alumni
if (isset($_POST['daftar'])){
    $nama   = $_POST['nama'];
    $hp     = $_POST['no_hp'];
    $email  = $_POST['email'];
    $jk     = $_POST['jenis_k'];
    $alamat = $_POST['alamat'];
    $status = $_POST['status'];

    if($nama!="" && $hp!="" && $email!="" && $jk!="" && $alamat!="" && $status!=""){
        $tambah = $func->tambahAnggota($nama, $email, $hp, $status, $alamat, $jk);
        if($tambah==true){
            $_SESSION['pesan']="Pendaftaran Berhasil";
            header("location:daftar.php?status=$status");
        }else{
            $_SESSION['pesan']="Pendaftaran gagal, Silahkan cek lagi data anda";
            header("location:daftar.php?status=$status");
        }
    }else{
        $_SESSION['pesan']="Maaf, tidak boleh ada data yang kosong";
        header("location:daftar.php?status=$status");
    }
}

//mendapatkan halaman berita
if(isset($_POST['halaman_tambah_berita'])){
    header('location:admin/tambah_berita.php');
}

//menambhakan berita
if(isset($_POST['tambah_berita'])){
    $judul  = $_POST['judul'];
    $isi    = $_POST['isi'];
    $kosong = 0;

    $fileName = $_FILES["foto"]["name"];
    $fileTmpLoc = $_FILES["foto"]["tmp_name"];
    $fileType = $_FILES["foto"]["type"];
    $fileSize = $_FILES["foto"]["size"];
    $fileErrorMsg = $_FILES["foto"]["error"];
//untuk penamaan foto di news
    $lastId = $func->getLastId("id_berita", "berita");
    $id = $lastId['id']+1;

    if($judul==""){
        $_SESSION['pesan'] .= "Judul berita belum ada <br>";
        $kosong = 1;
    }

    if($isi==""){
        $_SESSION['pesan'] .= "Isi berita belum ada <br>";
        $kosong = 1;
    }

    if($kosong==1){
        header("location:admin/tambah_berita.php");
    }else{
        if(!$fileTmpLoc){
            $_SESSION['pesan'] .= "Belum ada file yang dipilih <br>";
        }
        if($fileSize > 5242880) {
            $_SESSION['pesan'] .= "Ukuran file yang anda masukkan terlalu besar!<br>Ukuran maksimal peng-upload-tan foto 5 MB <br>";
        }
        if (!preg_match("/.(gif|jpg|png)$/i", $fileName) ) {
            $_SESSION['pesan'] .= "File yang anda masukkan bukan bertipe gambar! <br>";
        }
        if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
            $_SESSION['pesan'] .= "Terjadi masalah dalam peng-upload-an gambar! <br>";
        }

        $ext = pathinfo($fileName, PATHINFO_EXTENSION);
        $newName = "berita_".$id.".".$ext;
        $moveResult = move_uploaded_file($fileTmpLoc, "assets/images/news/$newName");

        if ($moveResult != true) {
            $_SESSION['pesan'] .= "Gagal mengupload foto <br>";
            header("location:admin/tambah_berita.php");
        }else{
            $cek = $func->tambahBerita($newName, $judul, $isi);
            if($cek==true){
                $_SESSION['pesan']="Berhasil menambahkan berita";
                header("location:admin/berita.php");
            }else{
                $_SESSION['pesan'] .= "Ada kesalahan saat menambahkan data <br>";
                header("location:admin/tambah_berita.php");
            }

        }
    }
}

//mendapatkan halaman edit berita
if(isset($_POST['halaman_edit_berita'])){
    $id = $_POST['id_berita'];
    header("location:admin/edit_berita.php?id=$id");
}

//edit berita
if(isset($_POST['edit_berita'])){
    $id = $_POST['id_berita'];
    $judul  = $_POST['judul'];
    $isi    = $_POST['isi'];
    $kosong = 0;

    $fileName = $_FILES["foto"]["name"];
    $fileTmpLoc = $_FILES["foto"]["tmp_name"];
    $fileType = $_FILES["foto"]["type"];
    $fileSize = $_FILES["foto"]["size"];
    $fileErrorMsg = $_FILES["foto"]["error"];

    if($judul==""){
        $_SESSION['pesan'] .= "Judul berita belum ada <br>";
        $kosong = 1;
    }

    if($isi==""){
        $_SESSION['pesan'] .= "Isi berita belum ada <br>";
        $kosong = 1;
    }

    if($kosong==1){
        header("location:admin/edit_berita.php?id=$id");
    }else{
        if($fileName==""){
            $cek = $func->editBerita($id, $fileName, $judul, $isi);
            if($cek==true){
                header("location:admin/berita.php");
            }else{
                $_SESSION['pesan'] .= "Ada kesalahan saat mengubah data <br>";
                header("location:admin/edit_berita.php?id=$id");
            }
        }else{
            if(!$fileTmpLoc){
                $_SESSION['pesan'] .= "Belum ada file yang dipilih <br>";
            }
            if($fileSize > 5242880) {
                $_SESSION['pesan'] .= "Ukuran file yang anda masukkan terlalu besar!<br>Ukuran maksimal peng-upload-tan foto 5 MB <br>";
            }
            if (!preg_match("/.(gif|jpg|png)$/i", $fileName) ) {
                $_SESSION['pesan'] .= "File yang anda masukkan bukan bertipe gambar! <br>";
            }
            if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
                $_SESSION['pesan'] .= "Terjadi masalah dalam peng-upload-an gambar! <br>";
            }

            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            $newName = "berita_".$id.".".$ext;
            $moveResult = move_uploaded_file($fileTmpLoc, "assets/images/news/$newName");

            if ($moveResult != true) {
                $_SESSION['pesan'] .= "Gagal mengupload foto <br>";
                header("location:admin/edit_berita.php?id=$id");
            }else{
                $cek = $func->editBerita($id, $newName, $judul, $isi);
                if($cek==true){
                    $_SESSION['pesan'] .= "Berhasil mengedit berita";
                    header("location:admin/berita.php");
                }else{
                    $_SESSION['pesan'] .= "Ada kesalahan saat mengubah data <br>";
                    header("location:admin/edit_berita.php?id=$id");
                }
            }
        }
    }
}

//hapus berita
if(isset($_POST['hapus_berita'])){
    $id = $_POST['id_berita'];

    $cek = $func->hapusBerita($id);
    if($cek==1){
        $_SESSION['pesan']="Berita berhasil dihapus";
    }else{
        $_SESSION['pesan']="Gagal menghapus berita";
    }

    header("location:admin/berita.php");
}

//ubah struktur
if(isset($_POST['ubah_struktur'])){
    $fileName = $_FILES["foto"]["name"];
    $fileTmpLoc = $_FILES["foto"]["tmp_name"];
    $fileType = $_FILES["foto"]["type"];
    $fileSize = $_FILES["foto"]["size"];
    $fileErrorMsg = $_FILES["foto"]["error"];

    if(!$fileTmpLoc){
        $_SESSION['pesan'] .= "Belum ada file yang dipilih <br>";
    }
    if($fileSize > 5242880) {
        $_SESSION['pesan'] .= "Ukuran file yang anda masukkan terlalu besar!<br>Ukuran maksimal peng-upload-tan foto 5 MB <br>";
    }
    if (!preg_match("/.(gif|jpg|png)$/i", $fileName) ) {
        $_SESSION['pesan'] .= "File yang anda masukkan bukan bertipe gambar! <br>";
    }
    if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
        $_SESSION['pesan'] .= "Terjadi masalah dalam peng-upload-an gambar! <br>";
    }

    $newName = "struktur.jpg";
    $moveResult = move_uploaded_file($fileTmpLoc, "assets/images/$newName");

    if ($moveResult != true) {
        $_SESSION['pesan'] .= "Gagal mengupload foto <br>";
        header("location:admin/pengurus.php");
    }else{
        $cek = $func->editStruktur($newName);
        if($cek==true){
            header("location:admin/pengurus.php");
        }else{
            $_SESSION['pesan'] .= "Ada kesalahan saat mengubah data <br>";
            header("location:admin/pengurus.php");
        }
    }
}

//ubah foto
if(isset($_POST['ubah_foto'])){
    $fileName = $_FILES["foto"]["name"];
    $fileTmpLoc = $_FILES["foto"]["tmp_name"];
    $fileType = $_FILES["foto"]["type"];
    $fileSize = $_FILES["foto"]["size"];
    $fileErrorMsg = $_FILES["foto"]["error"];

    if(!$fileTmpLoc){
        $_SESSION['pesan'] .= "Belum ada file yang dipilih <br>";
    }
    if($fileSize > 5242880) {
        $_SESSION['pesan'] .= "Ukuran file yang anda masukkan terlalu besar!<br>Ukuran maksimal peng-upload-tan foto 5 MB <br>";
    }
    if (!preg_match("/.(gif|jpg|png)$/i", $fileName) ) {
        $_SESSION['pesan'] .= "File yang anda masukkan bukan bertipe gambar! <br>";
    }
    if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
        $_SESSION['pesan'] .= "Terjadi masalah dalam peng-upload-an gambar! <br>";
    }

    $newName = "pengurus.jpg";
    $moveResult = move_uploaded_file($fileTmpLoc, "assets/images/$newName");

    if ($moveResult != true) {
        $_SESSION['pesan'] .= "Gagal mengupload foto <br>";
        header("location:admin/pengurus.php");
    }else{
        $cek = $func->editPengurus($newName);
        if($cek==true){
            header("location:admin/pengurus.php");
        }else{
            $_SESSION['pesan'] .= "Ada kesalahan saat mengubah data <br>";
            header("location:admin/pengurus.php");
        }
    }
}

//mencetak putera
if(isset($_POST['cetak_anggota'])){
    $jk = $_POST['jk'];
    if($jk=="putera"){
        header("location:admin/cetak_anggota.php?jk=1");
    }else if($jk=="puteri"){
        header("location:admin/cetak_anggota.php?jk=2");
    }

}

//menghapus anggota
if(isset($_POST['hapus_anggota'])){
    $id = $_POST['id'];

    $cek = $func->hapusAnggota($id);

    if($cek==1){
        $_SESSION['pesan']="Berhasil menghapus anggota";
    }else{
        $_SESSION['pesan']="Gagal menghapus anggota";
    }

    header('Location: ' . $_SERVER["HTTP_REFERER"] );
}

//verifikasi user
if(isset($_POST['verifikasi'])){
    $id = $_POST['id'];

    $cek = $func->verifikasiAnggota($id);

    if($cek==1){
        $_SESSION['pesan']="Verifikasi berhasil";
    }else{
        $_SESSION['pesan']="Verifikasi gagal";
    }

    header('Location: ' . $_SERVER["HTTP_REFERER"] );
}

//mendapatkan halaman galeri
if(isset($_POST['halaman_tambah_galeri'])){
    header('location:admin/tambah_galeri.php');
}

//menambhakan galeri
if(isset($_POST['tambah_galeri'])){
    $desk  = $_POST['deskripsi'];
    $kosong = 0;

    $fileName = $_FILES["foto"]["name"];
    $fileTmpLoc = $_FILES["foto"]["tmp_name"];
    $fileType = $_FILES["foto"]["type"];
    $fileSize = $_FILES["foto"]["size"];
    $fileErrorMsg = $_FILES["foto"]["error"];
//untuk penamaan foto
    $lastId = $func->getLastId("id_foto", "galeri");
    $id = $lastId['id']+1;

    if($desk==""){
        $_SESSION['pesan'] .= "Deskripsi foto belum ada <br>";
        $kosong = 1;
    }

    if($kosong==1){
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }else{
        if(!$fileTmpLoc){
            $_SESSION['pesan'] .= "Belum ada file yang dipilih <br>";
        }
        if($fileSize > 5242880) {
            $_SESSION['pesan'] .= "Ukuran file yang anda masukkan terlalu besar!<br>Ukuran maksimal peng-upload-tan foto 5 MB <br>";
        }
        if (!preg_match("/.(gif|jpg|png)$/i", $fileName) ) {
            $_SESSION['pesan'] .= "File yang anda masukkan bukan bertipe gambar! <br>";
        }
        if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
            $_SESSION['pesan'] .= "Terjadi masalah dalam peng-upload-an gambar! <br>";
        }

        $ext = pathinfo($fileName, PATHINFO_EXTENSION);
        $newName = "galeri_".$id.".".$ext;
        $moveResult = move_uploaded_file($fileTmpLoc, "assets/images/galeri/$newName");

        if ($moveResult != true) {
            $_SESSION['pesan'] .= "Gagal mengupload foto <br>";
            header('Location: ' . $_SERVER["HTTP_REFERER"] );
        }else{
            $cek = $func->tambahGaleri($newName, $desk);
            if($cek==true){
                $_SESSION['pesan']="Berhasil menambahkan galeri";
                header("location:admin/galeri.php");
            }else{
                $_SESSION['pesan'] .= "Ada kesalahan saat menambahkan data <br>";
                header('Location: ' . $_SERVER["HTTP_REFERER"] );
            }

        }
    }
}

//mendapatkan halaman edit galeri
if(isset($_POST['halaman_edit_galeri'])){
    $id = $_POST['id_galeri'];
    header("location:admin/edit_galeri.php?id=$id");
}

//edit galeri
if(isset($_POST['edit_galeri'])){
    $id = $_POST['id_galeri'];
    $desk    = $_POST['desk'];
    $kosong = 0;

    $fileName = $_FILES["foto"]["name"];
    $fileTmpLoc = $_FILES["foto"]["tmp_name"];
    $fileType = $_FILES["foto"]["type"];
    $fileSize = $_FILES["foto"]["size"];
    $fileErrorMsg = $_FILES["foto"]["error"];

    if($desk==""){
        $_SESSION['pesan'] .= "deskripsi galeri belum ada <br>";
        $kosong = 1;
    }

    if($kosong==1){
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }else{
        if($fileName==""){
            $cek = $func->editGaleri($id, $fileName, $desk);
            if($cek==true){
                header("location:admin/galeri.php");
            }else{
                $_SESSION['pesan'] .= "Ada kesalahan saat mengubah data <br>";
                header('Location: ' . $_SERVER["HTTP_REFERER"] );
            }
        }else{
            if(!$fileTmpLoc){
                $_SESSION['pesan'] .= "Belum ada file yang dipilih <br>";
            }
            if($fileSize > 5242880) {
                $_SESSION['pesan'] .= "Ukuran file yang anda masukkan terlalu besar!<br>Ukuran maksimal peng-upload-tan foto 5 MB <br>";
            }
            if (!preg_match("/.(gif|jpg|png)$/i", $fileName) ) {
                $_SESSION['pesan'] .= "File yang anda masukkan bukan bertipe gambar! <br>";
            }
            if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
                $_SESSION['pesan'] .= "Terjadi masalah dalam peng-upload-an gambar! <br>";
            }

            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            $newName = "galeri_".$id.".".$ext;
            $moveResult = move_uploaded_file($fileTmpLoc, "assets/images/galeri/$newName");

            if ($moveResult != true) {
                $_SESSION['pesan'] .= "Gagal mengupload foto <br>";
                header('Location: ' . $_SERVER["HTTP_REFERER"] );
            }else{
                $cek = $func->editGaleri($id, $newName, $desk);
                if($cek==true){
                    $_SESSION['pesan'] .= "Berhasil mengedit galeri";
                    header("location:admin/galeri.php");
                }else{
                    $_SESSION['pesan'] .= "Ada kesalahan saat mengubah data <br>";
                    header('Location: ' . $_SERVER["HTTP_REFERER"] );
                }
            }
        }
    }
}

//hapus galeri
if(isset($_POST['hapus_galeri'])){
    $id = $_POST['id_galeri'];

    $cek = $func->hapusGaleri($id);
    if($cek==1){
        $_SESSION['pesan']="Galeri berhasil dihapus";
    }else{
        $_SESSION['pesan']="Gagal menghapus berita";
    }
    header('Location: ' . $_SERVER["HTTP_REFERER"] );
}

//login
if(isset($_POST['login'])){
    $uname = $_POST['username'];
    $pass = $_POST['password'];

    if($uname!='' && $pass!=''){
        $cek = $func->cekUser($uname);
        if($cek <= 0){
            $_SESSION['pesan'] = 'Maaf, username tidak terdaftar';
            header('Location: ' . $_SERVER["HTTP_REFERER"] );
        }else{
            $password = md5($pass);
            $auth = $func->login($uname, $password);
            if($auth->num_rows > 0){
                $hasil = $auth->fetch_assoc();
                $_SESSION['uname'] = $hasil['username'];
                header('location:admin/index.php');
            }else{
                $_SESSION['pesan'] = 'Maaf, password salah';
                header('Location: ' . $_SERVER["HTTP_REFERER"] );
            }
        }
    }else{
        $_SESSION['pesan'] = 'Maaf, username atau password tidak boleh kosong';
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }
    
}

//logout
if(isset($_GET['act'])){
    if($_GET['act']=='logout'){
        unset($_SESSION['uname']);
        header('location:admin/login.php');
    }
}