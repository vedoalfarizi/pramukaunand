<?php
    include 'Fungsi.php';
    $func = new Fungsi();
    $func->cekSession();
    $func->auth();
    $id = $_SESSION['id'];
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Peserta Fiesta</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    FIESTA 2017
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="home.php">
                        <i class="pe-7s-graph"></i>
                        <p>Home</p>
                    </a>
                </li>
                <li class="active">
                    <a href="peserta.php">
                        <i class="pe-7s-user"></i>
                        <p>Daftarkan Peserta</p>
                    </a>
                </li>
                <li>
                    <a href="bidamping.php">
                        <i class="pe-7s-note2"></i>
                        <p>Daftarkan Bidamping</p>
                    </a>
                </li>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">Daftarkan 20 Peserta (10 Putera dan 10 Pueteri)</a>
                </div>
                <div class="collapse navbar-collapse">

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="controller/authController.php?act=logout">
                                <p>Log out</p>
                            </a>
                        </li>
						<li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <?php
                if($_SESSION['pesan']!=""){
                    echo "
                                            <div class=\"alert alert-info\">
                                                <span>".$_SESSION['pesan']."</span>
                                            </div>
                                        ";
                }
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Daftarkan Peserta</h4>
                            </div>
                            <div class="content">
                                <form method="post" action="controller/pesertaController.php" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Nama</label>
                                                <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>No KTA</label>
                                                <input type="text" name="kta" class="form-control" placeholder="NO KTA" >
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Jabatan/Golongan</label>
                                                <input type="text" name="gol" class="form-control" placeholder="Jabatan/Golongan">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Tempat Lahir</label>
                                                <input type="text" name="tempat" class="form-control" placeholder="Kota">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Tanggal Lahir</label>
                                                <input type="date" name="tgl" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <input type="hidden" name="id" value="<?php echo $id;?>">
                                <?php
                                    $jumlah = $func->jumlahPeserta($id);
                                    if($jumlah >= 20){
                                        echo '<a disabled class=\'btn btn-info pull-right\'>Penuh</a>';
                                    }else{
                                        echo '<button type=\'submit\' name=\'tambah_peserta\' class=\'btn btn-info btn-fill pull-right\'>Tambah</button>';
                                    }
                                ?>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-plain">
                            <div class="header">
                                <h4 class="title">Peserta Terdaftar</h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover">
                                    <thead>
                                        <th>No</th>
                                        <th>Nama</th>
                                        <th>No KTA</th>
                                        <th>Jab/Gol</th>
                                        <th>TTL</th>
                                        <th></th>
                                    </thead>
                                    <tbody>
                                <?php
                                    $pesertas = $func->showPeserta($id);
                                    $n = $pesertas->num_rows;
                                    if($n <= 0){
                                        echo '<tr>
                                                <td colspan="6">Belum ada peserta terdaftar</td>
                                              </tr>';
                                    }
                                    $no = 1;
                                    while($r = $pesertas->fetch_assoc()){
                                        echo '
                                        <tr>
                                            <td>'.$no.'</td>
                                            <td>'.$r['nama_peserta'].'</td>
                                            <td>'.$r['kta'].'</td>
                                            <td>'.$r['golongan'].'</td>
                                            <td>'.$r['tempat_lahir'].'/ '.$r['tgl'].'</td>
                                            <td>
                                                <form action="controller/pesertaController.php" method="POST">
                                                    <input type="hidden" name="id_peserta" value=\''.$r['id_peserta'].'\'>
                                                    <td><button type="submit" name="hapus_peserta" onclick="return confirm(\'Anda yakin ingin menghapus peserta?\');" class="btn btn-info btn-fill pull-left">Hapus</button></td>
                                                </form>
                                            </td>
                                        </tr>      
                                        ';
                                        $no++;
                                    }
                                ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Upload Scan Surat Rekomendasi KWARCAB</h4>
                                <h6>Bagi yang tidak memiliki KTA</h6>
                            </div>
                            <div class="content">
                                <form action="controller/pesertaController.php" method="POST" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>
                                            <?php
                                                $surat = $func->getRekomendasiUser($id);
                                                if($surat!=NULL){
                                                    echo 'Sudah mengupload surat';
                                                }else{
                                                    echo 'Belum mengupload surat';
                                                }
                                            ?>
                                                </label>
                                                <input type="file" name="foto" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <input type="hidden" name="id_user" value="<?php echo $id ;?>">
                                            <button type="submit" name="surat_rekomendasi" class="btn btn-info btn-fill pull-right">Upload</button>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Upload Foto Peserta</h4>
                                <h6>FORMAT : Masing-masing foto diberi nama NamaSiswa.jpg <br> kemudian disatukan dalam file NamaSekolah.zip</h6>
                            </div>
                            <div class="content">
                                <form action="controller/pesertaController.php" method="POST" enctype="multipart/form-data">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>
                                                    <?php
                                                    $surat = $func->getFotoUser($id);
                                                    if($surat!=NULL){
                                                        echo 'Sudah mengupload foto';
                                                    }else{
                                                        echo 'Belum mengupload foto';
                                                    }
                                                    ?>
                                                </label>
                                                <input type="hidden" name="id_user" value="<?php echo $id ;?>">
                                                <input type="file" name="foto">
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <button type="submit" name="foto_peserta" class="btn btn-info btn-fill pull-right">Upload</button>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <footer class="footer">
            <div class="copyright pull-right">
                &copy; 2017, made with <i class="fa fa-heart heart"></i> by <a href="http://www.creative-tim.com" target="_blank">Creative Tim</a>
                & <a href="http://neotelemetri.com" target="_blank">Neotelemetri</a>
            </div>
        </footer>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="assets/js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>

</html>

<?php $func->clearSession() ?>
