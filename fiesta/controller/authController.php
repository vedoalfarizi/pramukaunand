<?php
include '../Fungsi.php';
$func = new Fungsi();
$func->cekSession();

    if(isset($_POST['daftar'])){
        $ns     = $_POST['nama_sekolah'];
        $email  = $_POST['email'];
        $hp     = $_POST['no_hp'];
        $pass   = $_POST['password'];

        if($ns != '' && $email !='' && $hp !='' && $pass !=''){
            $cekuser = $func->cekUser($email);
            if($cekuser > 0){
                $_SESSION['pesan'] = 'Maaf, Anda sudah terdaftar';
                header('location:../index.php#registrasi');
            }else{
                $password = md5($pass);
                $daftar = $func->daftar($ns, $email, $hp, $password);
                if($daftar){
                    $_SESSION['pesan'] = 'Pendaftaran berhasil';
                    header('location:../index.php#registrasi');
                }else{
                    $_SESSION['pesan'] = 'Maaf, ada kesalahan saat mendaftar';
                    header('location:../index.php#registrasi');
                }
            }
        }else{
            $_SESSION['pesan'] = 'Maaf, Anda belum mengisi semua form';
            header('location:../index.php#registrasi');
        }
    }

    if(isset($_POST['login'])){
        $email = $_POST['email'];
        $pass = $_POST['password'];

        if($email !='' && $pass !=''){
            $cekuser = $func->cekUser($email);
            if($cekuser > 0){
                $password = md5($pass);
                $login = $func->login($email, $password);
                if($login->num_rows > 0){
                    $k = $login->fetch_assoc();
                    $_SESSION['id'] = $k['id_user'];
                    header('location:../home.php');
                }else{
                    $_SESSION['pesan'] = 'Maaf, Email atau Password salah';
                    header('location:../index.php#login');    
                }
            }else{
                $_SESSION['pesan'] = 'Maaf, Anda belum terdaftar';
                header('location:../index.php#login');
            }
        }else{
            $_SESSION['pesan'] = 'Maaf, Email atau Password tidak boleh kosong';
            header('location:../index.php#login');
        }
    }

    if(isset($_POST['login_fiesta'])){
        $uname = $_POST['username'];
        $pass = $_POST['password'];

        if($uname !='' && $pass !=''){
            $password = md5($pass);
            $login = $func->loginAdmin($uname, $password);
            if($login->num_rows > 0){
                $k = $login->fetch_assoc();
                $_SESSION['id_admin'] = $k['id_admin'];
                $_SESSION['uname_admin'] = $k['username'];
                header('location:../admin/home.php');
            }else{
                $_SESSION['pesan'] = 'Maaf, Username atau Password salah';
                header('Location: ' . $_SERVER["HTTP_REFERER"] );
            }
        }else{
            $_SESSION['pesan'] = 'Maaf, Username atau Password tidak boleh kosong';
            header('Location: ' . $_SERVER["HTTP_REFERER"] );
        }
    }


    if(isset($_GET['act'])){
        if($_GET['act']=='logout'){
            unset($_SESSION['id']);
            header('location:../index.php');
        }else if($_GET['act']=='logoutadmin'){
            unset($_SESSION['id_admin']);
            header('location:../admin/index.php');
        }
    }
?>