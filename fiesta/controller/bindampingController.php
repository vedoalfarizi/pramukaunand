<?php
/**
 * Created by PhpStorm.
 * User: vedo_alfarizi
 * Date: 26/07/2017
 * Time: 22.14
 */
include '../Fungsi.php';
$func = new Fungsi();
$func->cekSession();

if(isset($_POST['tambah_bindamping'])){
    $id = $_POST['id'];
    $nama = $_POST['nama'];

    if($nama===''){
        $_SESSION['pesan'] .= "Mohon lengkapi semua data";
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }else{
        $cek = $func->tambahBindamping($nama, $id);
        if($cek==true){
            header("location:../bidamping.php");
        }else{
            $_SESSION['pesan'] .= "Ada kesalahan saat menambah bindamping <br>";
            header('Location: ' . $_SERVER["HTTP_REFERER"] );
        }
    }
}

if(isset($_POST['hapus_bindamping'])){
    $id = $_POST['id_bindamping'];
    $hapus = $func->hapusBindamping($id);
    if($hapus){
        header("location:../bidamping.php");
    }else{
        $_SESSION['pesan'] .= "Ada kesalahan saat menghapus bindamping <br>";
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }
}
?>