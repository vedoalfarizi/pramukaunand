<?php
/**
 * Created by PhpStorm.
 * User: vedo_alfarizi
 * Date: 27/07/2017
 * Time: 02.49
 */
include '../Fungsi.php';
$func = new Fungsi();
$func->cekSession();

if(isset($_POST['add_news'])){
    $judul  = $_POST['judul'];
    $desk    = $_POST['desk'];
    $kosong = 0;

    $fileName = $_FILES["foto"]["name"];
    $fileTmpLoc = $_FILES["foto"]["tmp_name"];
    $fileType = $_FILES["foto"]["type"];
    $fileSize = $_FILES["foto"]["size"];
    $fileErrorMsg = $_FILES["foto"]["error"];
//untuk penamaan foto di news
    $lastId = $func->getLastId("id_news", "news");
    $id = $lastId['id']+1;

    if($judul==""){
        $_SESSION['pesan'] .= "Judul berita belum ada <br>";
        $kosong = 1;
    }

    if($desk==""){
        $_SESSION['pesan'] .= "Isi berita belum ada <br>";
        $kosong = 1;
    }

    if($kosong==1){
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }else{
        if(!$fileTmpLoc){
            $_SESSION['pesan'] .= "Belum ada file yang dipilih <br>";
        }
        if($fileSize > 5242880) {
            $_SESSION['pesan'] .= "Ukuran file yang anda masukkan terlalu besar!<br>Ukuran maksimal peng-upload-tan foto 2 MB <br>";
        }
        if (!preg_match("/.(gif|jpg|png)$/i", $fileName) ) {
            $_SESSION['pesan'] .= "File yang anda masukkan bukan bertipe gambar! <br>";
        }
        if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
            $_SESSION['pesan'] .= "Terjadi masalah dalam peng-upload-an gambar! <br>";
        }

        $ext = pathinfo($fileName, PATHINFO_EXTENSION);
        $newName = "news_".$id.".".$ext;
        $moveResult = move_uploaded_file($fileTmpLoc, "../news/$newName");

        if ($moveResult != true) {
            $_SESSION['pesan'] .= "Gagal mengupload foto <br>";
            header('Location: ' . $_SERVER["HTTP_REFERER"] );
        }else{
            $cek = $func->tambahBerita($desk, $newName, $judul);
            if($cek==true){
                $_SESSION['pesan']="Berhasil menambahkan berita";
                header('Location: ' . $_SERVER["HTTP_REFERER"] );
            }else{
                $_SESSION['pesan'] .= "Ada kesalahan saat menambahkan data <br>";
                header('Location: ' . $_SERVER["HTTP_REFERER"] );
            }

        }
    }
}

if(isset($_POST['halaman_edit_berita'])){
    $id = $_POST['id_news'];
    header("location:../admin/edit_news.php?id=$id");
}

if(isset($_POST['edit_berita'])){
    $id = $_POST['id_news'];
    $judul  = $_POST['judul'];
    $desk    = $_POST['desk'];
    $kosong = 0;

    $fileName = $_FILES["foto"]["name"];
    $fileTmpLoc = $_FILES["foto"]["tmp_name"];
    $fileType = $_FILES["foto"]["type"];
    $fileSize = $_FILES["foto"]["size"];
    $fileErrorMsg = $_FILES["foto"]["error"];

    if($judul==""){
        $_SESSION['pesan'] .= "Judul berita belum ada <br>";
        $kosong = 1;
    }

    if($desk==""){
        $_SESSION['pesan'] .= "Isi berita belum ada <br>";
        $kosong = 1;
    }

    if($kosong==1){
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }else{
        if($fileName==""){
            $cek = $func->editBerita($id, $fileName, $judul, $desk);
            if($cek==true){
                header("location:../admin/news.php");
            }else{
                $_SESSION['pesan'] .= "Ada kesalahan saat mengubah data <br>";
                header('Location: ' . $_SERVER["HTTP_REFERER"] );
            }
        }else{
            if(!$fileTmpLoc){
                $_SESSION['pesan'] .= "Belum ada file yang dipilih <br>";
            }
            if($fileSize > 5242880) {
                $_SESSION['pesan'] .= "Ukuran file yang anda masukkan terlalu besar!<br>Ukuran maksimal peng-upload-tan foto 5 MB <br>";
            }
            if (!preg_match("/.(gif|jpg|png)$/i", $fileName) ) {
                $_SESSION['pesan'] .= "File yang anda masukkan bukan bertipe gambar! <br>";
            }
            if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
                $_SESSION['pesan'] .= "Terjadi masalah dalam peng-upload-an gambar! <br>";
            }

            $ext = pathinfo($fileName, PATHINFO_EXTENSION);
            $newName = "news_".$id.".".$ext;
            $moveResult = move_uploaded_file($fileTmpLoc, "../news/$newName");

            if ($moveResult != true) {
                $_SESSION['pesan'] .= "Gagal mengupload foto <br>";
                header('Location: ' . $_SERVER["HTTP_REFERER"] );
            }else{
                $cek = $func->editBerita($id, $newName, $judul, $desk);
                if($cek==true){
                    $_SESSION['pesan'] .= "Berhasil mengedit berita";
                    header("location:../admin/news.php");
                }else{
                    $_SESSION['pesan'] .= "Ada kesalahan saat mengubah data <br>";
                    header('Location: ' . $_SERVER["HTTP_REFERER"] );
                }
            }
        }
    }
}

if(isset($_POST['hapus_berita'])){
    $id = $_POST['id_news'];

    $cek = $func->hapusBerita($id);
    if($cek==1){
        $_SESSION['pesan']="Berita berhasil dihapus";
    }else{
        $_SESSION['pesan']="Gagal menghapus berita";
    }

    header('Location: ' . $_SERVER["HTTP_REFERER"] );
}
?>