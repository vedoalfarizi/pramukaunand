<?php
include '../Fungsi.php';
$func = new Fungsi();
$func->cekSession();

if(isset($_POST['bukti'])){
    $id     = $_POST['id'];
    $nama   = $_POST['nama'];

    $fileName = $_FILES["foto"]["name"];
    $fileTmpLoc = $_FILES["foto"]["tmp_name"];
    $fileType = $_FILES["foto"]["type"];
    $fileSize = $_FILES["foto"]["size"];
    $fileErrorMsg = $_FILES["foto"]["error"];
    $ext = pathinfo($fileName, PATHINFO_EXTENSION);

    if(!$fileTmpLoc){
        $_SESSION['pesan'] .= "Belum ada file yang dipilih <br>";
    }
    if($fileSize > 5242880) {
        $_SESSION['pesan'] .= "Ukuran file yang anda masukkan terlalu besar!<br>Ukuran maksimal peng-upload-tan foto 2 MB <br>";
    }
    if (!preg_match("/.(gif|jpg|png)$/i", $fileName) ) {
        $_SESSION['pesan'] .= "File yang anda masukkan bukan bertipe gambar! <br>";
    }
    if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
        $_SESSION['pesan'] .= "Terjadi masalah dalam peng-upload-an gambar! <br>";
    }

    $newName = $nama.'_'.$id.'.'.$ext;
    $moveResult = move_uploaded_file($fileTmpLoc, "../upload/bukti/$newName");

    if ($moveResult != true) {
        $_SESSION['pesan'] .= "Gagal mengupload bukti <br>";
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }else{
        $upload = $func->uploadBukti($newName, $id);
        if($upload==true){
            $_SESSION['pesan']="Berhasil mengupload bukti";
            header('Location: ' . $_SERVER["HTTP_REFERER"] );
        }else{
            $_SESSION['pesan'] .= "Ada kesalahan saat mengupload bukti <br>";
            header('Location: ' . $_SERVER["HTTP_REFERER"] );
        }

    }
}

if(isset($_POST['mandat'])){
    $id     = $_POST['id'];
    $nama   = $_POST['nama'];

    $fileName = $_FILES["foto"]["name"];
    $fileTmpLoc = $_FILES["foto"]["tmp_name"];
    $fileType = $_FILES["foto"]["type"];
    $fileSize = $_FILES["foto"]["size"];
    $fileErrorMsg = $_FILES["foto"]["error"];
    $ext = pathinfo($fileName, PATHINFO_EXTENSION);

    if(!$fileTmpLoc){
        $_SESSION['pesan'] .= "Belum ada file yang dipilih <br>";
    }
    if($fileSize > 5242880) {
        $_SESSION['pesan'] .= "Ukuran file yang anda masukkan terlalu besar!<br>Ukuran maksimal peng-upload-tan foto 2 MB <br>";
    }
    if (!preg_match("/.(rar|zip)$/i", $fileName) ) {
        $_SESSION['pesan'] .= "File yang anda masukkan bukan bertipe gambar! <br>";
    }
    if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
        $_SESSION['pesan'] .= "Terjadi masalah dalam peng-upload-an gambar! <br>";
    }

    $newName = $nama.'_'.$id.'.'.$ext;
    $moveResult = move_uploaded_file($fileTmpLoc, "../upload/mandat dan pernyataan/$newName");

    if ($moveResult != true) {
        $_SESSION['pesan'] .= "Gagal mengupload file <br>";
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }else{
        $upload = $func->uploadMandatPernyataan($newName, $id);
        if($upload==true){
            $_SESSION['pesan']="Berhasil mengupload file";
            header('Location: ' . $_SERVER["HTTP_REFERER"] );
        }else{
            $_SESSION['pesan'] .= "Ada kesalahan saat mengupload file <br>";
            header('Location: ' . $_SERVER["HTTP_REFERER"] );
        }

    }
}

if(isset($_POST['upload_juknis'])){
    $fileName = $_FILES["foto"]["name"];
    $fileTmpLoc = $_FILES["foto"]["tmp_name"];
    $fileType = $_FILES["foto"]["type"];
    $fileSize = $_FILES["foto"]["size"];
    $fileErrorMsg = $_FILES["foto"]["error"];
    $ext = pathinfo($fileName, PATHINFO_EXTENSION);
    $tahun = date("Y");


    if(!$fileTmpLoc){
        $_SESSION['pesan'] .= "Belum ada file yang dipilih <br>";
    }
    if($fileSize >= 8388608) {
        $_SESSION['pesan'] .= "Ukuran file yang anda masukkan terlalu besar!<br>Ukuran maksimal peng-upload-tan foto 8 MB <br>";
    }
    if (!preg_match("/.(gif|jpg|png)$/i", $fileName) ) {
        $_SESSION['pesan'] .= "File yang anda masukkan bukan bertipe gambar! <br>";
    }
    if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
        $_SESSION['pesan'] .= "Terjadi masalah dalam peng-upload-an gambar! <br>";
    }

    $newName = 'juknis_'.$tahun.'.'.$ext;
    $moveResult = move_uploaded_file($fileTmpLoc, "../upload/juknis/$newName");

    if ($moveResult != true) {
        $_SESSION['pesan'] .= "Gagal mengupload file <br>";
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }else{
        $upload = $func->uploadJuknis($newName);
        if($upload==true){
            $_SESSION['pesan']="Berhasil mengupload file";
            header('Location: ' . $_SERVER["HTTP_REFERER"] );
        }else{
            $_SESSION['pesan'] .= "Ada kesalahan saat mengupload file <br>";
            header('Location: ' . $_SERVER["HTTP_REFERER"] );
        }

    }
}

?>