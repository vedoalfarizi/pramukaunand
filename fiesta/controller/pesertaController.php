<?php
include '../Fungsi.php';
$func = new Fungsi();
$func->cekSession();

if(isset($_POST['tambah_peserta'])){
    $id = $_POST['id'];
    $nama = $_POST['nama'];
    $kta = $_POST['kta'];
    $gol = $_POST['gol'];
    $tempat = $_POST['tempat'];
    $tgl = $_POST['tgl'];

    if($nama==='' || $gol==='' || $tempat==='' || $tgl===''){
        $_SESSION['pesan'] .= "Mohon lengkapi semua data";
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }else{
        $cek = $func->tambahPeserta($nama, $kta, $tempat, $tgl, $gol, $id);
        if($cek==true){
            header("location:../peserta.php");
        }else{
            $_SESSION['pesan'] .= "Ada kesalahan saat menambah peserta <br>";
            header('Location: ' . $_SERVER["HTTP_REFERER"] );
        }
    }
}

if(isset($_POST['hapus_peserta'])){
    $id = $_POST['id_peserta'];
    $hapus = $func->hapusPeserta($id);
    if($hapus){
        header("location:../peserta.php");
    }else{
        $_SESSION['pesan'] .= "Ada kesalahan saat menghapus peserta <br>";
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }
}

if(isset($_POST['surat_rekomendasi'])){
    $id = $_POST['id_user'];
    $nama   = $func->getNamaSekolah($id);

    $fileName = $_FILES["foto"]["name"];
    $fileTmpLoc = $_FILES["foto"]["tmp_name"];
    $fileType = $_FILES["foto"]["type"];
    $fileSize = $_FILES["foto"]["size"];
    $fileErrorMsg = $_FILES["foto"]["error"];
    $ext = pathinfo($fileName, PATHINFO_EXTENSION);

    if(!$fileTmpLoc){
        $_SESSION['pesan'] .= "Belum ada file yang dipilih <br>";
    }
    if($fileSize > 5242880) {
        $_SESSION['pesan'] .= "Ukuran file yang anda masukkan terlalu besar!<br>Ukuran maksimal peng-upload-tan file 2 MB <br>";
    }
    if (!preg_match("/.(gif|jpg|png)$/i", $fileName) ) {
        $_SESSION['pesan'] .= "File yang anda masukkan bukan bertipe foto! <br>";
    }
    if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
        $_SESSION['pesan'] .= "Terjadi masalah dalam peng-upload-an gambar! <br>";
    }

    $newName = $nama.'_'.$id.'.'.$ext;
    $moveResult = move_uploaded_file($fileTmpLoc, "../upload/rekomendasi/$newName");

    if ($moveResult != true) {
        $_SESSION['pesan'] .= "Gagal mengupload surat <br>";
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }else{
        $upload = $func->uploadSuratRekomendasi($newName, $id);
        if($upload==true){
            $_SESSION['pesan']="Berhasil mengupload surat";
            header('Location: ' . $_SERVER["HTTP_REFERER"] );
        }else{
            $_SESSION['pesan'] .= "Ada kesalahan saat mengupload surat <br>";
            header('Location: ' . $_SERVER["HTTP_REFERER"] );
        }

    }
}

if(isset($_POST['foto_peserta'])){
    $id = $_POST['id_user'];
    $nama   = $func->getNamaSekolah($id);

    $fileName = $_FILES["foto"]["name"];
    $fileTmpLoc = $_FILES["foto"]["tmp_name"];
    $fileType = $_FILES["foto"]["type"];
    $fileSize = $_FILES["foto"]["size"];
    $fileErrorMsg = $_FILES["foto"]["error"];
    $ext = pathinfo($fileName, PATHINFO_EXTENSION);

    if(!$fileTmpLoc){
        $_SESSION['pesan'] .= "Belum ada file yang dipilih <br>";
    }
    if($fileSize > 5242880) {
        $_SESSION['pesan'] .= "Ukuran file yang anda masukkan terlalu besar!<br>Ukuran maksimal peng-upload-tan file 2 MB <br>";
    }
    if (!preg_match("/.(gif|jpg|png)$/i", $fileName) ) {
        $_SESSION['pesan'] .= "File yang anda masukkan bukan bertipe foto! <br>";
    }
    if ($fileErrorMsg == 1) { // if file upload error key is equal to 1
        $_SESSION['pesan'] .= "Terjadi masalah dalam peng-upload-an gambar! <br>";
    }

    $newName = $nama.'_'.$id.'.'.$ext;
    $moveResult = move_uploaded_file($fileTmpLoc, "../upload/foto/$newName");

    if ($moveResult != true) {
        $_SESSION['pesan'] .= "Gagal mengupload foto <br>";
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }else{
        $upload = $func->uploadFotoPeserta($newName, $id);
        if($upload==true){
            $_SESSION['pesan']="Berhasil mengupload foto";
            header('Location: ' . $_SERVER["HTTP_REFERER"] );
        }else{
            $_SESSION['pesan'] .= "Ada kesalahan saat mengupload foto <br>";
            header('Location: ' . $_SERVER["HTTP_REFERER"] );
        }

    }
}

if(isset($_POST['lihat_peserta'])){
    $id = $_POST['id_user'];
    header('location:../admin/detail.php?id_p='.$id.'');
}

if(isset($_POST['verifikasi'])){
    $id = $_POST['id_user'];
    $veri = $func->verifikasiPeserta($id);
    if($veri){
        $_SESSION['pesan']="Berhasil memverifikasi peserta";
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }else{
        $_SESSION['pesan']="Gagal memverifikasi peserta";
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }
}

if(isset($_POST['batalkan_peserta'])){
    $id = $_POST['id_user'];
    $veri = $func->batalkanVerifikasi($id);
    if($veri){
        $_SESSION['pesan']="Berhasil membatalkan verifikasi peserta";
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }else{
        $_SESSION['pesan']="Gagal membatakan verifikasi peserta";
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }
}

if(isset($_POST['hapus_user'])){
    $id = $_POST['id_user'];
    $hapus = $func->hapusUser($id);
    if($hapus){
        $_SESSION['pesan']="Berhasil menghapus peserta";
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }else{
        $_SESSION['pesan']="Gagal menghapus peserta";
        header('Location: ' . $_SERVER["HTTP_REFERER"] );
    }
}
?>