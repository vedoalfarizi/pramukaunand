<?php

    class Fungsi
    {
        private $host   = 'localhost';
        private $user   = 'root';
        private $pass   = '';
        private $db     = 'fiesta';

        private function connect()
        {
            $kon = new mysqli($this->host, $this->user, $this->pass, $this->db);

            return $kon;
        }

        public function daftar($username, $email, $hp, $password)
        {   
            $kon = $this->connect();
            $query = $kon->query('INSERT INTO user(`nama_sekolah`, `email`, `no_hp`, `password`) VALUES(\''.$username.'\', \''.$email.'\',\''.$hp.'\', \''.$password.'\')');

            return $query;

            $kon->close();
        }

        public function login($email, $pass)
        {
            $kon = $this->connect();
            $query = $kon->query('SELECT id_user FROM user WHERE email=\''.$email.'\' AND password=\''.$pass.'\'');

            return $query;

            $kon->close();
        }

        public function loginAdmin($uname, $pass)
        {
            $kon = $this->connect();
            $query = $kon->query('SELECT * FROM admin WHERE username=\''.$uname.'\' AND password=\''.$pass.'\'');

            return $query;

            $kon->close();
        }

        public function auth()
        {   
            if(!isset($_SESSION['id'])){
                header('location:index.php');
            }
        }

        public function authAdmin()
        {
            if(!isset($_SESSION['id_admin'])){
                header('location:index.php');
            }
        }

        public function cekUser($email)
        {
            $kon = $this->connect();
            $query = $kon->query('SELECT COUNT(id_user) AS jumlah FROM user WHERE email=\''.$email.'\'');
            $k = $query->fetch_assoc();
            $jumlah = $k['jumlah'];

            return $jumlah;

            $kon->close();
        }

        public function cekPesan()
        {
            if($_SESSION['pesan']!=''){
                $v = true;
            }else{
                $v = false;
            }

            return $v;
        }

        public function cekSession()
        {
            if(!isset($_SESSION)){
                session_start();
            }

            if(!isset($_SESSION['pesan'])){
                $_SESSION['pesan'] = '';
            }
        }

        public function clearSession()
        {
            unset($_SESSION['pesan']);
        }

        public function getNamaSekolah($id)
        {
            $kon = $this->connect();
            $query = $kon->query('SELECT nama_sekolah FROM user WHERE id_user=\''.$id.'\'');
            $hasil = $query->fetch_assoc();
            return $hasil['nama_sekolah'];

            $kon->close();
        }

        public function getStatusUser($id)
        {
            $kon = $this->connect();
            $query = $kon->query('SELECT status_verifikasi FROM user WHERE id_user=\''.$id.'\'');
            $hasil = $query->fetch_assoc();
            return $hasil['status_verifikasi'];

            $kon->close();
        }

        public function getBuktiUser($id)
        {
            $kon = $this->connect();
            $query = $kon->query('SELECT foto_bukti FROM user WHERE id_user=\''.$id.'\'');
            $hasil = $query->fetch_assoc();
            return $hasil['foto_bukti'];

            $kon->close();
        }

        public function getRekomendasiUser($id)
        {
            $kon = $this->connect();
            $query = $kon->query('SELECT surat_rekomendasi FROM user WHERE id_user=\''.$id.'\'');
            $hasil = $query->fetch_assoc();
            return $hasil['surat_rekomendasi'];

            $kon->close();
        }

        public function getFotoUser($id)
        {
            $kon = $this->connect();
            $query = $kon->query('SELECT foto_peserta FROM user WHERE id_user=\''.$id.'\'');
            $hasil = $query->fetch_assoc();
            return $hasil['foto_peserta'];

            $kon->close();
        }

        public function getMandat($id)
        {
            $kon = $this->connect();
            $query = $kon->query('SELECT surat_mandat_pernyataan FROM user WHERE id_user=\''.$id.'\'');
            $hasil = $query->fetch_assoc();
            return $hasil['surat_mandat_pernyataan'];

            $kon->close();
        }

        public function uploadBukti($file, $id)
        {
            $kon = $this->connect();
            $query = $kon->query('UPDATE user SET foto_bukti=\''.$file.'\' WHERE id_user=\''.$id.'\'');

            return $query;

            $kon->close();
        }

        public function uploadMandatPernyataan($file, $id)
        {
            $kon = $this->connect();
            $query = $kon->query('UPDATE user SET surat_mandat_pernyataan=\''.$file.'\' WHERE id_user=\''.$id.'\'');

            return $query;

            $kon->close();
        }

        public function uploadJuknis($file)
        {
            $kon = $this->connect();
            $query = $kon->query('INSERT INTO `juknis`(`nama_file`) VALUES (\''.$file.'\')');

            return $query;

            $kon->close();
        }

        public function getJuknis()
        {
            $kon = $this->connect();
            $query = $kon->query('SELECT * FROM `juknis` WHERE id=(SELECT MAX(id) FROM juknis)');
            $data = $query->fetch_assoc();

            return $data['nama_file'];

            $kon->close();
        }

        public function uploadSuratRekomendasi($file, $id)
        {
            $kon = $this->connect();
            $query = $kon->query('UPDATE user SET surat_rekomendasi=\''.$file.'\' WHERE id_user=\''.$id.'\'');

            return $query;

            $kon->close();
        }

        public function uploadFotoPeserta($file, $id)
        {
            $kon = $this->connect();
            $query = $kon->query('UPDATE user SET foto_peserta=\''.$file.'\' WHERE id_user=\''.$id.'\'');

            return $query;

            $kon->close();
        }

        public function tambahPeserta($nama, $kta, $tempat, $tgl, $gol, $id)
        {
            $kon = $this->connect();
            $query = $kon->query('INSERT INTO peserta(`nama_peserta`, `kta`, `tempat_lahir`, `tgl`, `golongan`, `user_id`) VALUES(\''.$nama.'\', \''.$kta.'\', \''.$tempat.'\', \''.$tgl.'\', \''.$gol.'\', \''.$id.'\')');

            return $query;

            $kon->close();
        }

        public function showPeserta($id){
            $kon = $this->connect();
            $query = $kon->query('SELECT * FROM peserta WHERE user_id=\''.$id.'\'');

            return $query;

            $kon->close();
        }

        public function jumlahVerifikasi($status){
            $kon = $this->connect();
            if($status == 'belum'){
                $query = $kon->query('SELECT id_user FROM user WHERE status_verifikasi=0');
            }else if($status == 'sudah'){
                $query = $kon->query('SELECT id_user FROM user WHERE status_verifikasi=1');
            }
            $jumlah = $query->num_rows;

            return $jumlah;

            $kon->close();
        }

        public function showAllVerifikasi($status, $start , $end)
        {
            $kon = $this->connect();
            if($status == 'belum'){
                $query = $kon->query('SELECT * FROM user WHERE status_verifikasi=0 LIMIT '.$start.','.$end.'');
            }else if($status == 'sudah'){
                $query = $kon->query('SELECT * FROM user WHERE status_verifikasi=1 LIMIT '.$start.','.$end.'');
            }


            return $query;
            $kon->close();
        }

        public function verifikasiPeserta($id){
            $kon = $this->connect();
            $query = $kon->query('UPDATE user SET status_verifikasi=1 WHERE id_user=\''.$id.'\'');

            return $query;

            $kon->close();
        }

        public function batalkanVerifikasi($id){
            $kon = $this->connect();
            $query = $kon->query('UPDATE user SET status_verifikasi=0 WHERE id_user=\''.$id.'\'');

            return $query;

            $kon->close();
        }

        public function hapusPeserta($id){
            $kon = $this->connect();
            $query = $kon->query('DELETE FROM peserta WHERE id_peserta=\''.$id.'\'');

            return $query;

            $kon->close();
        }

        public function jumlahPeserta($id){
            $kon = $this->connect();
            $query = $kon->query('SELECT id_peserta FROM peserta WHERE user_id=\''.$id.'\'');
            $jumlah = $query->num_rows;

            return $jumlah;

            $kon->close();
        }

        public function tambahBindamping($nama, $id)
        {
            $kon = $this->connect();
            $query = $kon->query('INSERT INTO bindamping(`nama_bindamping`, `user_id`) VALUES(\''.$nama.'\', \''.$id.'\')');

            return $query;

            $kon->close();
        }

        public function hapusBindamping($id){
            $kon = $this->connect();
            $query = $kon->query('DELETE FROM bindamping WHERE id_bindamping=\''.$id.'\'');

            return $query;

            $kon->close();
        }

        public function showBindamping($id){
            $kon = $this->connect();
            $query = $kon->query('SELECT * FROM bindamping WHERE user_id=\''.$id.'\'');

            return $query;

            $kon->close();
        }

        public function jumlahBindamping($id){
            $kon = $this->connect();
            $query = $kon->query('SELECT id_bindamping FROM bindamping WHERE user_id=\''.$id.'\'');
            $jumlah = $query->num_rows;

            return $jumlah;

            $kon->close();
        }

        public function jumlahAllBindamping(){
            $kon = $this->connect();
            $query = $kon->query('SELECT id_bindamping FROM bindamping');
            $jumlah = $query->num_rows;

            return $jumlah;

            $kon->close();
        }

        public function jumlahAllBindampingVerified(){
            $kon = $this->connect();
            $query = $kon->query('SELECT bindamping.id_bindamping FROM bindamping, user WHERE bindamping.user_id = user.id_user AND user.status_verifikasi=1');
            $jumlah = $query->num_rows;

            return $jumlah;

            $kon->close();
        }

        public function jumlahAllPeserta(){
            $kon = $this->connect();
            $query = $kon->query('SELECT id_peserta FROM peserta');
            $jumlah = $query->num_rows;

            return $jumlah;

            $kon->close();
        }

        public function jumlahAllSekolah(){
            $kon = $this->connect();
            $query = $kon->query('SELECT id_user FROM user');
            $jumlah = $query->num_rows;

            return $jumlah;

            $kon->close();
        }

        public function jumlahAllPesertaVerified(){
            $kon = $this->connect();
            $query = $kon->query('SELECT peserta.id_peserta FROM peserta, user WHERE peserta.user_id = user.id_user AND user.status_verifikasi=1');
            $jumlah = $query->num_rows;

            return $jumlah;

            $kon->close();
        }

        public function jumlahAllSekolahVerified(){
            $kon = $this->connect();
            $query = $kon->query('SELECT id_user FROM user WHERE status_verifikasi=1');
            $jumlah = $query->num_rows;

            return $jumlah;

            $kon->close();
        }

        function showBerita(){
            $kon = $this->connect();
            $query = $kon->query('SELECT * FROM news');

            return $query;
            $kon->close();
        }

        public function showSomeBerita($start , $end)
        {
            $kon = $this->connect();
            $query = $kon->query('SELECT * FROM news LIMIT '.$start.','.$end.'');

            return $query;
            $kon->close();
        }

        function getLastId($kolom, $tabel){
            $kon = $this->connect();
            $lastId = $kon->query('SELECT MAX(\''.$kolom.'\') AS n FROM \''.$tabel.'\'');

            return $lastId;
            $kon->close();
        }

        function tambahBerita($desk, $foto, $judul){
            $kon = $this->connect();
            $query = $kon->query("INSERT INTO news(`deskripsi`, `foto`, `judul`) VALUES('$desk','$foto','$judul')");

            return $query;
            $kon->close();
        }

        function showOneBerita($id){
            $kon = $this->connect();
            $query = $kon->query("SELECT * FROM news WHERE id_news='$id'");

            return $query;
            $kon->close();
        }

        function editBerita($id, $foto, $judul, $desk){
            $kon = $this->connect();
            if($foto==""){
                $query = $kon->query("UPDATE news SET judul='$judul', deskripsi='$desk' WHERE id_news='$id'");
            }else{
                $query = $kon->query("UPDATE news SET judul='$judul', deskripsi='$desk', foto='$foto' WHERE id_news='$id'");
            }

            return $query;
            $kon->close();
        }

        function hapusBerita($id){
            $kon = $this->connect();
            $query = $kon->query("DELETE FROM news WHERE id_news='$id'");

            return $query;
            $kon->close();
        }

        function hapusUser($id){
            $kon = $this->connect();
            $query = $kon->query("DELETE FROM user WHERE id_user='$id'");

            return $query;
            $kon->close();
        }

        function getDeadline($id){
            $kon = $this->connect();
            $query = $kon->query("SELECT terdaftar FROM user WHERE id_user='$id'");
            $data = $query->fetch_assoc();
            $tgl = $data['terdaftar'];
            $due = date('Y-m-d', strtotime($tgl.'+7 days'));

            return $due;
            $kon->close();
        }
    }
    
?>