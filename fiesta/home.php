<?php
    include 'Fungsi.php';
    $func = new Fungsi();
    $func->cekSession();
    $func->auth();
    $id = $_SESSION['id'];
    $status = $func->getStatusUser($id);
?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="assets/img/favicon.ico">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Peserta Fiesta</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">

    <!--

        Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
        Tip 2: you can also add an image using data-image tag

    -->

    	<div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    FIESTA 2017
                </a>
            </div>

            <ul class="nav">
                <li class="active">
                    <a href="home.php">
                        <i class="pe-7s-graph"></i>
                        <p>Home</p>
                    </a>
                </li>
            <?php
                if($status == 1){
                    echo '
                       <li>
                            <a href="peserta.php">
                                <i class="pe-7s-user"></i>
                                <p>Daftarkan Peserta</p>
                            </a>
                        </li>
                        <li>
                            <a href="bidamping.php">
                                <i class="pe-7s-note2"></i>
                                <p>Daftarkan Bidamping</p>
                            </a>
                        </li>                    
                    ';
                }
            ?>
            </ul>
    	</div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">Selamat Datang <?php echo $func->getNamaSekolah($id) ?></a>
                </div>
                <div class="collapse navbar-collapse">

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="controller/authController.php?act=logout">
                                <p>Log out</p>
                            </a>
                        </li>
						<li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
    <?php
        if($status == 0){
            $bukti = $func->getBuktiUser($id);
            echo '
                <div class=\'row\'>
                    <div class=\'col-md-7\'>
                        <div class=\'card\'>';
                            if($_SESSION['pesan']!=''){
                                echo '<div class=\'alert alert-info\'>
                                        <span>'.$_SESSION['pesan'].'</span>
                                    </div>';
                            }
                            echo '<div class=\'header\'>
                                <h4 class=\'title\'>Upload Bukti Pembayaran</h4>
                            </div>
                            <div class=\'content\'>
                                <form action=\'controller/homeController.php\' method=\'post\' enctype=\'multipart/form-data\'>
                                    <div class=\'row\'>
                                        <div class=\'col-md-5\'>
                                            <div class=\'form-group\'>
                                                <label>';
                                                if($bukti!=NULL){
                                                    echo 'Sudah mengupload bukti';
                                                }else{
                                                    echo 'Belum mengupload bukti <br> FORMAT: NamaSekolah.JPG';
                                                }
                                                echo '</label>
                                                <input type=\'file\' name=\'foto\' class=\'form-control\'>
                                                <input type=\'hidden\' name=\'nama\' value=\''.$func->getNamaSekolah($id).'\'>
                                                <input type=\'hidden\' name=\'id\' value=\''.$id.'\'>
                                            </div>
                                        </div>
                                    </div>';
                                    if($func->jumlahAllSekolahVerified() < 50){
                                        if(date('Y-m-d') <= $func->getDeadline($id)){
                                            echo '<button type=\'submit\' name=\'bukti\' class=\'btn btn-info btn-fill pull-left\'>Upload</button>';
                                        }else{
                                            echo '<a disabled="" class="btn btn-info btn-fill pull-left">Deadline</a>';
                                        }
                                    }else{
                                        echo '<a disabled="" class="btn btn-info btn-fill pull-left">Kuota Penuh</a>';
                                    }

                                    echo '<div class=\'clearfix\'></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Status <b>Belum Diverifikasi</b></h4>
                                <h5 class="text-danger title">Deadline Pembayaran : '.$func->getDeadline($id).'</h5>
                            </div>
                            <div class="content">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div>
                                            <label>1. Upload Bukti Pembayaran</label>
                                            <label>2. Admin akan memverifikasi bukti pembayaran</label>
                                            <label>3. Silahkan Hubungi CP jika terjadi masalah</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            ';
        }else if($status == 1){
            $mandat = $func->getMandat($id);
            echo '
                <div class=\'row\'>
                    <div class=\'col-md-7\'>
                        <div class=\'card\'>';
                if($_SESSION['pesan']!=''){
                    echo '
                        <div class=\'alert alert-info\'>
                            <span>'.$_SESSION['pesan'].'</span>
                        </div>';
                }
                    echo '
                        <div class=\'header\'>
                            <h4 class=\'title\'>Upload Surat Mandat (Bindamping dan Peserta) dan Pernytaan Kesediaan Ikut Serta dan Gudep</h4>
                        </div>
                        <div class=\'content\'>
                            <form action=\'controller/homeController.php\' method=\'post\' enctype=\'multipart/form-data\'>
                                <div class=\'row\'>
                                    <div class=\'col-md-12\'>
                                        <div class=\'form-group\'>
                                            <label>';
                                            if($mandat!=NULL){
                                                echo 'Sudah mengupload mandat dan Pernyataan';
                                            }else{
                                                echo 'Belum mengupload mandat dan Pernyataan';
                                            }
                                            echo '</label><br>
                                            <label><b>FORMAT : NamaSekolah.ZIP/RAR</b></label>
                                            <input type=\'file\' name=\'foto\' class=\'form-control\'>
                                            <input type=\'hidden\' name=\'nama\' value=\''.$func->getNamaSekolah($id).'\'>
                                            <input type=\'hidden\' name=\'id\' value=\''.$id.'\'>
                                        </div>
                                    </div>
                                </div>

                                <button type=\'submit\' name=\'mandat\' class=\'btn btn-info btn-fill pull-left\'>Upload</button>
                                <div class=\'clearfix\'></div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Status <b>Sudah Diverifikasi</b></h4>
                        </div>
                        <div class="content">
                            <div class="row">
                                <div class="col-md-12">
                                    <div>
                                        <label>1. Daftarkan peserta dan Bindamping dengan melengkapi berkas</label>
                                        <label>2. Upload Surat Mandat serta Pernyataan Kesediaan Ikut Serta dan Gudep</label>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class=\'row\'>
                <div class=\'col-md-5\'>
                    <div class=\'card\'>
                        <div class=\'header\'>
                            <h4 class=\'title\'>Download Juknis</h4>
                        </div>
                        <div class=\'content\'>
                            <a class=\'btn btn-info\' href=\'upload/juknis.zip\'>Download</a>
                        </div>
                    </div>
                </div>
            </div>
            ';

        }
    ?>
                
            </div>
        </div>


        <footer class="footer">
            <div class="copyright pull-right">
                &copy; 2017, made with <i class="fa fa-heart heart"></i> by <a href="http://www.creative-tim.com" target="_blank">Creative Tim</a>
                & <a href="http://neotelemetri.com" target="_blank">Neotelemetri</a>
            </div>
        </footer>

    </div>
</div>


</body>

    <!--   Core JS Files   -->
    <script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
	<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

	<!--  Charts Plugin -->
	<script src="assets/js/chartist.min.js"></script>

    <!--  Notifications Plugin    -->
    <script src="assets/js/bootstrap-notify.js"></script>

    <!--  Google Maps Plugin    -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="assets/js/light-bootstrap-dashboard.js"></script>

	<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
	<script src="assets/js/demo.js"></script>

</html>

<?php $func->clearSession() ?>
