<?php
/**
 * Created by PhpStorm.
 * User: Labdas SI
 * Date: 7/28/2017
 * Time: 2:08 AM
 */
include '../Fungsi.php';
$func = new Fungsi();
$func->cekSession();
$func->authAdmin();
$id = $_SESSION['id_admin'];
$uname = $_SESSION['uname_admin'];
$id_p = $_GET['id_p'];
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="../assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Admin Fiesta</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="../assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="../assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="../assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="../assets/css/demo.css" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="../assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-5.jpg">

        <!--

            Tip 1: you can change the color of the sidebar using: data-color="blue | azure | green | orange | red | purple"
            Tip 2: you can also add an image using data-image tag

        -->

        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="http://www.creative-tim.com" class="simple-text">
                    FIESTA 2017
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="home.php">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li>
                    <a href="news.php">
                        <i class="pe-7s-user"></i>
                        <p>News</p>
                    </a>
                </li>
                <li>
                    <a href="peserta.php">
                        <i class="pe-7s-note2"></i>
                        <p>Peserta</p>
                    </a>
                </li>
                <li>
                    <a href="verifikasi.php">
                        <i class="pe-7s-note2"></i>
                        <p>Verifikasi Peserta</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand">Selamat Datang <?php echo $uname; ?></a>
                </div>
                <div class="collapse navbar-collapse">

                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="../controller/authController.php?act=logoutadmin">
                                <p>Log out</p>
                            </a>
                        </li>
                        <li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
               <a class="btn btn-info btn-fill" href="javascript:history.back()">Kembali</a>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Peserta <?php echo $func->getNamaSekolah($id_p);?></h4>
                                <?php
                                if($_SESSION['pesan']!=""){
                                    echo "
                                                <div class=\"alert alert-success\">
                                                    <span>".$_SESSION['pesan']."</span>
                                                </div>
                                            ";
                                }
                                ?>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>No</th>
                                        <th>Nama Peserta</th>
                                        <th>No KTA</th>
                                        <th>TTL</th>
                                        <th>Golongan</th>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $pesertas = $func->showPeserta($id_p);

                                    $no=1;
                                    while($peserta = $pesertas->fetch_assoc()){
                                        echo "
                                                    <tr>
                                                        <td>".$no."</td>
                                                        <td>".$peserta['nama_peserta']."</td>
                                                        <td>".$peserta['kta']."</td>
                                                        <td>".$peserta['tempat_lahir']." / ".$peserta['tgl']."</td>
                                                        <td>".$peserta['golongan']."</td>
                                                    </tr>  
                                                ";
                                        $no++;
                                    }
                                    ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Bindamping <?php echo $func->getNamaSekolah($id_p);?></h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                    <th>No</th>
                                        <th>Nama Bindamping</th>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $pesertas = $func->showBindamping($id_p);

                                    $no=1;
                                    while($peserta = $pesertas->fetch_assoc()){
                                        echo "
                                                    <tr>
                                                        <td>".$no."</td>
                                                        <td>".$peserta['nama_bindamping']."</td>
                                                    </tr>  
                                                ";
                                        $no++;
                                    }
                                    ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <footer class="footer">
            <div class="copyright pull-right">
                &copy; 2017, made with <i class="fa fa-heart heart"></i> by <a href="http://www.creative-tim.com" target="_blank">Creative Tim</a>
                & <a href="http://neotelemetri.com" target="_blank">Neotelemetri</a>
            </div>
        </footer>

    </div>
</div>


</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!--  Google Maps Plugin    -->
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>

</html>

<?php $func->clearSession();?>

