<?php
include 'Fungsi.php';
$func = new Fungsi();
$func->cekSession();
if(isset($_SESSION['id'])){
    header('location:home.php');
}
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>FIESTA 2017</title>

    <!-- Bootstrap Core CSS -->
    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link href="css/agency.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js" integrity="sha384-0s5Pv64cNZJieYFkXYOTId2HMA2Lfb6q2nAcx2n0RTLUnCAoTTsS0nKEO27XyKcY" crossorigin="anonymous"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js" integrity="sha384-ZoaMbDF+4LeFxg6WdScQ9nnR1QC2MIRxA1O9KWEXQwns1G8UNyIEZIQidzb0T1fo" crossorigin="anonymous"></script>
    <![endif]-->

</head>

<body id="home" class="index">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Menu Navigasi</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a href="#home"><img src="img/racana.PNG" style="height:50px;margin-top:-5px" /></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#home"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#news">News</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#registrasi">Registration</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#login">Login</a>
                    </li>
                    <li>
                    <?php
                        $file = $func->getJuknis();
                        echo '
                            <a href="upload/juknis/'.$file.'" >Download Juknis</a>
                        ';
                    ?>
                    </li>

                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text" style="color: #0b0b0b">
                <div class="intro-lead-in">&nbsp;</div>
                <div class="intro-heading">&nbsp;</div>
            </div>
        </div>
    </header>

    <!-- Portfolio Grid Section -->
    <section id="news" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">News</h2>
                    <h3 class="section-subheading text-muted">News and Event in FIESTA 2017</h3>
                </div>
            </div>
            <div class="row">
            <?php
                $data_per_page = 10;
                $n_all_data =  $func->showBerita()->num_rows;

                if(!isset($_GET['page'])){
                    $page = 1;
                }else{
                    $page = $_GET['page'];
                }

                $start = ($page-1)*$data_per_page;

                $beritas = $func->showSomeBerita($start , $data_per_page);

                $all_page = ceil($n_all_data/$data_per_page);

                while($berita = $beritas->fetch_assoc()){
                    echo '
                        <div class=\'col-md-4 col-sm-6 portfolio-item\'>
                            
                                <img src=\'news/'.$berita['foto'].'\' class=\'img-responsive\' alt=\'\'>
                            
                            <div class=\'portfolio-caption\'>
                                <h4>'.$berita['judul'].'</h4>
                                <p class=\'text-muted\'>'.$berita['deskripsi'].'</p>
                            </div>
                        </div>
                    ';
                }
            ?>

            </div>
        </div>
        <div class="container-fluid">
            <?php
            for($page = 1; $page <= $all_page; $page++){
                echo '| <a href=\'index.php#login?page='.$page.'\'>'.$page.'</a> | ';
            }
            ?>
        </div>
    </section>

    <!-- About Section -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">About</h2>
                    <h3 class="section-subheading text-muted">FIESTA UNAND 2017</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/1.png" alt="">
                            </div>
                            <div class="timeline-panel">
        
                                <div class="timeline-body">
                                    <p class="text-muted">Gerakan pramuka adalah wadah pendidikan generasi muda yang menerapkan Dasa Dharma dan Tri Satya sebagai landasannya. Ia tumbuh dan berkembang ditengah-tengah tradisi bangsa Indonesia yang terus berupaya menanamkan kemandirian moral, ilmu pengetahuan dan penghayatan serta pengamalan nilai-nilai kesatria dan patriotisme. Dewasa ini keberadaan Pramuka terus dipertahankan dan dikembangkan, agar memiliki kontribusi lebih besar dalam proses pembelajaran dan pendidikan anak bangsa terutama generasi muda.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/2.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                
                                <div class="timeline-body">
                                    <p class="text-muted">Universitas Andalas memiliki Gugus depan aktif yang telah berdiri sejak 11 Maret 1985 di Solok, dan selanjutnya disahkan menjadi Racana Swarnadwipa Universitas Andalas pada 1 September 1986 oleh Ka. Mabida Sumatera Barat pada saat itu. Sejak berdirinya 31 tahun yang lalu. Prestasi-prestasi yang di raih Pramuka Universitas Andalas tak perlu di ragukan lagi baik tingkat Daerah, Nasional maupun Internasional. Pramuka Universitas Andalas selalu aktif dalam perkemahan perguruan tinggi tingkat Nasional. Selain itu Pramuka Universitas Andalas menjadi salah satu organisasi penghasil pelopor Duta Pemuda Sumatera Barat baik Duta Pariwisata dan Duta Bahari. Pramuka Universitas Andalas juga pernah meraih Dana Hibah Rp 40.000.000,- dari Program Hibah Bina Desa Kemenristek Dikti Tahun 2015 dengan kegiatan meningkatan sentra tebu di kenagarian Bukik Batuah, Kecamatan Canduang, Kabupaten Agam.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/3.png" alt="">
                            </div>
                            <div class="timeline-panel">
                               
                                <div class="timeline-body">
                                    <p class="text-muted">Sebagai wujud syukur atas nikmat dan rahmat Allah SWT yang berupa kemajuan dalam menunaikan visi dan misi Gerakan Pramuka dengan Kode Kehormatan Pramuka, maka Racana Putera Puteri Swarnadwipa Universitas Andalas mengadakan Peringatan HUT GUDEP XXXI. Peringatan ini juga merupakan salah satu evaluasi organisasi baik internal maupun eksternal, guna mengukur dan mengetahui kemajuan Gerakan Pramuka sebagaimana ditekankan oleh para pendiri Gerakan Pramuka tidak hanya dengan  melihat proses pendidikan peserta didik, melainkan juga harus dari aplikasinya di dalam masyarakat.</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/2.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                
                                <div class="timeline-body">
                                    <p class="text-muted">Sebuah kata bijak mungkin bisa kita jadikan motivasi pada diri kita, “Jangan tanyakan apa yang negara berikan kepada kita, tapi tanyakan apa yang bisa kita berikan kepada negara.”(John F.Kenedy). Atas dasar itu, sebagai generasi muda hendaknya kita dapat mengingat kembali semangat generasi tempo dulu kepada generasi muda penerus bangsa. Salah satu cara yang dapat dilakukan adalah dengan peningkatan karakter, dan bersinergi untuk meraih prestasi yang cemerlang.</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/about/3.png" alt="">
                            </div>
                            <div class="timeline-panel">
                                
                                <div class="timeline-body">
                                    <p class="text-muted">Dengan acara peringatan ini, diharapkan dapat dijadikan dasar pola pembinaan dan pengembangan Gerakan Pramuka khususnya yang berpangkalan di Perguruan Tinggi serta kelangsungannya di masa yang akan datang. Harus disadari bahwa Gerakan Pramuka merupakan wadah pendidikan generasi muda yang menyalurkan aspirasi masyarakat melalui pengabdian dengan penekanan pada pembinaan mental (kepribadian) dan pengajaran ilmu pengetahuan sesuai gebrakan pemerintah yaitu Revolusi Mental.<p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <h4>Let's
                                    <br>Join
                                    <br>With Us!</h4>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- Registration Section -->
    <section id="registrasi">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">REGISTRATION</h2>
                    <?php
                        if($_SESSION['pesan']!=''){
                            echo '<h3 class=\'section-subheading text-muted\'>'.$_SESSION['pesan'].'</h3>';
                        }else{
                            echo '<h3 class=\'section-subheading text-muted\'>Silahkan daftarkan sekolahmu.</h3>';
                        }
                    ?>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <form action="controller/authController.php" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" name="nama_sekolah" class="form-control" placeholder="Nama Sekolah" id="name" required data-validation-required-message="Silahkan masukkan nama sekolah Anda.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Email" id="email" required data-validation-required-message="Silahkan masukkan alamat email Anda.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="number" name="no_hp" class="form-control" placeholder="No. Handphone" id="phone" required data-validation-required-message="Silahkan masukkan nomor handphone Anda.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="Password" id="phone" required data-validation-required-message="Silahkan masukkan password Anda.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                           
                            <div class="clearfix"></div>
                        <?php
                            if($func->jumlahAllSekolahVerified() < 50){
                                echo '
                                    <div class="col-lg-12 text-center">
                                        <div id="success"></div>
                                        <button type="submit" name="daftar" class="btn btn-xl">Daftar</button>
                                    </div>
                                ';
                            }else{
                                echo '
                                    <div class="col-lg-12 text-center">
                                        <a class="btn btn-xl" disabled="">Peserta Sudah Penuh</a>
                                    </div>
                                ';
                            }
                        ?>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>


    <!-- Login Section -->
    <section id="login" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">Login Sekolah</h2>
                <?php
                    if($_SESSION['pesan']!=''){
                        echo '<h3 class=\'section-subheading text-muted\'>'.$_SESSION['pesan'].'</h3>';
                    }
                ?>
                </div>
            </div>
            <div class="row">

                <form action="controller/authController.php" method="POST">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Email" id="email" required data-validation-required-message="Silahkan masukkan alamat email Anda.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="password" name="password" class="form-control" placeholder="Password" id="phone" required data-validation-required-message="Silahkan masukkan password Anda.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                           
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" name="login" class="btn btn-xl">Login</button>
                            </div>
                        </div>
                    </form>
            </div>
            
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; FIESTA 2017</span>
                </div>
            </div>
        </div>
    </footer>

    <!-- jQuery -->
    <script src="vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" integrity="sha384-mE6eXfrb8jxl0rzJDBRanYqgBxtJ6Unn4/1F7q4xRRyIw7Vdg9jP4ycT7x1iVsgb" crossorigin="anonymous"></script>

    <!-- Contact Form JavaScript -->
    <script src="js/jqBootstrapValidation.js"></script>
    <script src="js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/agency.min.js"></script>

</body>

</html>
<?php 
    $func->clearSession();
?>