
<!-- komnentar dari vedo -->
<!DOCTYPE html>
<!--comment-->
<html>
<head>
    <meta charset="utf-8">
    <title>Pramuka Unand</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href="assets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">

    <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600' rel='stylesheet' type='text/css'>


    <link rel="shortcut icon" href="assets/images/logo-racana.png">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">

    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
</head>
<body>

<div id="header-row">
	<div class="container">
        <div class="row" style="text-align: center; color:white;">
            <div class="col-1"><a class="brand" href="#"><img src="assets/images/logo pramuka.png"/ style="max-width: 50px"></a></div>
            <div class="col-9" style="font-family: 'Verdana', Helvetica, Arial, sans-serif; font-size: 200%; line-height: 100%;">RACANA PUTERA PUTERI SWARNADWIPA <br> GUDEP PADANG 06.067-06.068 <br> UNIVERSITAS ANDALAS</div>
            <div class="col-1"><a class="brand" href="#"><img src="assets/images/logo-racana.png" style="max-width: 110px"></a></div>
        </div>
		<div class="row" style="text-align: center">
            <div class="col-3"></div>
			<div class="col-6">
				<div id="navigasi" class="navbar">
					<div class="navbar-default">
						<a data-target=".navbar-responsive-collapse" data-toggle="collapse" class="btn btn-navbar"><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></a>
						<div class="nav-collapse collapse navbar-responsive-collapse">
							<ul class="nav">
								<li><a href="index.php">Beranda</a></li>
								<li><a href="galeri.php">Galeri</a></li>
								<li><a href="fiesta/index.php">Fiesta</a></li>
								<li class="active"><a href="about.php">Tentang</a></li>
								<li class="dropdown">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">Anggota <b class="caret"></b></a>
									<ul class="dropdown-menu">
										<li><a href="pengurus.php">Pengurus</a></li>
										<li><a href="anggota.php">Anggota</a></li>
										<li><a href="alumni.php">Alumni</a></li>
									</ul>
								</li>
								<li><a href="berita.php">Berita</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<div class="container">
    <div class="col-6"><img src="assets/images/logo.jpg" alt="sejarah pramuka" width="100%"></div>
    <div class="noborder col-5 text-justify">
        <p>Awal tahun 1908 Baden Powell menulis pengalamannya untuk acara latihan kepramukaan yang dirintisnya. Kumpulan tulisannya ini dibuat buku dengan judul “Scouting For Boys”. Buku ini cepat tersebar di Inggris dan negara-negara lain yang kemudian berdiri organisasi kepramukaan yang semula hanya untuk laki-laki dengan nama Boys Scout.</p>
        <p> Tahun 1912 atas bantuan adik perempuan beliau, Agnes didirikan organisasi kepramukaan untuk wanita dengan nama Girl Guides yang kemudian diteruskan oleh istri beliau.</p>
        <p>Tahun 1916 berdiri kelompok pramuka usia siaga dengan nama CUB (anak serigala) dengan buku The Jungle Book karangan Rudyard Kipling sebagai pedoman kegiatannya. Buku ini bercerita tentang Mowgli si anak rimba yang dipelihara di hutan oleh induk serigala.</p>
        <p>Tahun 1918 beliau membentuk Rover Scout bagi mereka yang telah berusia 17 tahun. Tahun 1922 beliau menerbitkan buku Rovering To Success (Mengembara Menuju Bahagia). Buku ini menggambarkan seorang pemuda yang harus mengayuh sampannya menuju ke pantai bahagia.</p>
        <p>Tahun 1920 diselenggarakan Jambore Dunia yang pertama di Olympia Hall, London. Beliau mengundang pramuka dari 27 Negara dan pada saat itu Baden Powell diangkat sebagai Bapak Pandu Sedunia (Chief Scout of The World).</p>
    </div>
</div>

<?php include 'footer.php';?>

</body>
</html>
