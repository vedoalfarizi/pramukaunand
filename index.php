<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Pramuka Unand</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">

	<link href="assets/css/bootstrap.css" rel="stylesheet">
	<link href="assets/css/bootstrap-responsive.css" rel="stylesheet">
	<link href="assets/css/style.css" rel="stylesheet">

	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600' rel='stylesheet' type='text/css'>


	<link rel="shortcut icon" href="assets/images/logo-racana.png">
	<link rel="apple-touch-icon-precomposed" sizes="144x144" href="ico/apple-touch-icon-144-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="114x114" href="ico/apple-touch-icon-114-precomposed.png">
	<link rel="apple-touch-icon-precomposed" sizes="72x72" href="ico/apple-touch-icon-72-precomposed.png">
	<link rel="apple-touch-icon-precomposed" href="ico/apple-touch-icon-57-precomposed.png">

</head>
<body>

 <?php include'header.php'; ?>

<div class="container">

	<div id="myCarousel" class="carousel slide" style="margin-bottom: -9%; margin-top: 5%">
		<div class="carousel-inner">
			<div class="active item">
				<div class="container">
					<div class="row">
						<div class="span3">
<!--                            supaya ke tengah-->
						</div>
						<div class="span6"> <img src="assets/images/slide/1.JPG"></div>
					</div>
				</div>
			</div>
			<div class="item">
				<div class="container">
					<div class="row">
						<div class="span3">
                            <!--                            supaya ke tengah-->
                        </div>
					<div class="span6"> <img src="assets/images/slide/2.JPG"></div>
					</div>
				</div>
			</div>
            <div class="item">
                <div class="container">
                    <div class="row">
                        <div class="span3">
                            <!--                            supaya ke tengah-->
                        </div>
                        <div class="span6"> <img src="assets/images/slide/3.JPG"></div>
                    </div>
                </div>
            </div>
            <div class="item">
                <div class="container">
                    <div class="row">
                        <div class="span3">
                            <!--                            supaya ke tengah-->
                        </div>
                        <div class="span6"> <img src="assets/images/slide/4.JPG"></div>
                    </div>
                </div>
            </div>
		</div>

		<a class="carousel-control left " href="#myCarousel" data-slide="prev"><i class="icon-chevron-left"></i></a>
		<a class="carousel-control right" href="#myCarousel" data-slide="next"><i class="icon-chevron-right"></i></a>

	</div>
</div>

<?php include 'footer.php';?>

 <script src="assets/js/jquery.js"></script>
 <script src="assets/js/bootstrap.min.js"></script>
 <script src="assets/js/style.js"></script>

</body>
</html>
