<?php
session_start();
/**
 * Created by PhpStorm.
 * User: vedo_alfarizi
 * Date: 06/07/2017
 * Time: 21.48
 */
include '../Fungsi.php';
$func = new Fungsi();
$func->cekSession();

 if(isset($_SESSION['uname'])){
    header('location:index.php');
 }
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="../assets/images/logo-racana.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Racana Swarnadwipa</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

</head>
<body>
<nav class="navbar navbar-ct-purple navbar-absolute">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a class="navbar-brand" href="../index.php">Racana Swarnadwipa</a>
        </div>
    </div>
</nav>

<div class="wrapper">
    <div class="header header-filter" style="background-image: url('../assets/images/struktur_3.jpg'); background-size: cover; background-position: top center;">
        <div class="container">
            <br><br><br><br><br>    
            <div class="row">
                <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                    <div class="card card-signup">
                        <form class="form" method="post" action="../route.php">
                            <div class="header header-primary text-center">
                                <h4>Login</h4>
                                <h6>
                                <?php
                                if($_SESSION['pesan']!=""){
                                    echo "
                                            <div class=\"alert alert-success\">
                                                <span>".$_SESSION['pesan']."</span>
                                            </div>
                                        ";
                                }
                                ?>
                                </h6>       
                            </div>
                            <div class="content">

                                <div class="input-group">
										<span class="input-group-addon">
											<i class="pe-7s-user"></i>
										</span>
                                    <input type="text" name="username" class="form-control" placeholder="Username...">
                                </div>

                                <div class="input-group">
										<span class="input-group-addon">
											<i class="pe-7s-key"></i>
										</span>
                                    <input type="password" name="password" class="form-control" placeholder="Password...">
                                </div>

                            </div>
                            <div class="footer text-center">
                                <button type="submit" name="login" class="btn btn-simple btn-primary btn-lg">Login</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <footer class="footer">
            <div class="container">
                <div class="copyright pull-right">
                    &copy; 2017, made with <i class="fa fa-heart heart"></i> by <a href="http://www.creative-tim.com" target="_blank">Creative Tim</a>
                    & <a href="http://neotelemetri.com" target="_blank">Neotelemetri</a>
                </div>
            </div>
        </footer>

    </div>

</div>
</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

</html>

<?php $func->clearSession();?>
