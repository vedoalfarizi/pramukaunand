<?php
/**
 * Created by PhpStorm.
 * User: vedo_alfarizi
 * Date: 05/07/2017
 * Time: 20.07
 */
include "../Fungsi.php";
$func = new Fungsi();
$func->cekSession();
$func->auth();

$id = $_GET['id'];
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="../assets/images/logo-racana.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Racana Swarnadwipa</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="assets/css/animate.min.css" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="assets/css/light-bootstrap-dashboard.css" rel="stylesheet"/>

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />

    <link rel="stylesheet" href="assets/css/style.css">
</head>
<body>

<div class="wrapper">
    <div class="sidebar" data-color="purple" data-image="assets/img/sidebar-1.jpg">
        <div class="sidebar-wrapper">
            <div class="logo">
                <a href="#" class="simple-text">
                    <img src="../assets/images/logo-racana.png" alt="">
                </a>
            </div>

            <ul class="nav">
                <li>
                    <a href="index.php">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li class="active">
                    <a href="galeri.php">
                        <i class="pe-7s-news-paper"></i>
                        <p>Galeri</p>
                    </a>
                </li>
                <li>
                    <a href="berita.php">
                        <i class="pe-7s-news-paper"></i>
                        <p>Berita</p>
                    </a>
                </li>
                <li>
                    <a href="pengurus.php">
                        <i class="pe-7s-user"></i>
                        <p>Pengurus</p>
                    </a>
                </li>
                <li>
                    <a href="anggota_pa.php">
                        <i class="pe-7s-user"></i>
                        <p>Anggota Putera</p>
                    </a>
                </li>
                <li>
                    <a href="anggota_pi.php">
                        <i class="pe-7s-user"></i>
                        <p>Anggota Puteri</p>
                    </a>
                </li>
                <li>
                    <a href="verifikasi.php">
                        <i class="pe-7s-user"></i>
                        <p>Verifikasi Anggota</p>
                    </a>
                </li>
            </ul>
        </div>
    </div>

    <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Galeri</a>
                </div>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a href="../route.php?act=logout">
                                <p>Log out</p>
                            </a>
                        </li>
                        <li class="separator hidden-lg hidden-md"></li>
                    </ul>
                </div>
            </div>
        </nav>


        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Edit Galeri</h4>
                                <?php
                                if($_SESSION['pesan']!=""){
                                    echo "
                                            <div class=\"alert alert-danger\">
                                                <span>".$_SESSION['pesan']."</span>
                                            </div>
                                        ";
                                }

                                $galeri = $func->showOneGaleri($id)->fetch_assoc();
                                ?>
                            </div>
                            <div class="content">
                                <form action="../route.php" method="post" enctype="multipart/form-data">

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Deskripsi</label>
                                                <textarea rows="2" class="form-control" placeholder="Tulis deskripsi galeri disini" name="desk"><?php echo $galeri['deskripsi']?></textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-5">
                                            <div class="form-group">
                                                <label>Foto</label>
                                                <input type="file" name="foto" class="form-control">
                                            </div>
                                        </div>
                                    </div>

                                    <input type="hidden" name="id_galeri" value="<?php echo $id?>">

                                    <button type="submit" name="edit_galeri" class="btn btn-info btn-fill pull-right">Ubah</button>
                                    <div class="clearfix"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <footer class="footer">
            <div class="container-fluid">
                <p class="copyright pull-right">
                    &copy; <script>document.write(new Date().getFullYear())</script> <a href="http://www.creative-tim.com">Creative Tim</a> & <a href="http://neotelemetri.com" target="_blank">Neotelemetri</a>, made with love for a better web
                </p>
            </div>
        </footer>

    </div>
</div>


</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!--  Charts Plugin -->
<script src="assets/js/chartist.min.js"></script>

<!--  Notifications Plugin    -->
<script src="assets/js/bootstrap-notify.js"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js"></script>

<!-- Light Bootstrap Table DEMO methods, don't include it in your project! -->
<script src="assets/js/demo.js"></script>



<?php $func->clearSession();?>
