<?php
/**
 * Created by PhpStorm.
 * User: vedo_alfarizi
 * Date: 04/07/2017
 * Time: 15.17
 */
include "../Fungsi.php";
$func = new Fungsi();
$func->cekSession();
$func->auth();

$jk = $_GET['jk'];
?>

<!doctype html>
<html lang="en">
<head>
    <script>window.print()</script>

    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="../assets/images/logo-racana.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

    <title>Racana Swarnadwipa</title>

    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />


    <!-- Bootstrap core CSS     -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" />

    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="assets/css/pe-icon-7-stroke.css" rel="stylesheet" />
</head>
<body>

<div class="wrapper">
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title text-center">Daftar Anggota
                                    <?php
                                    if($jk==1){
                                        echo "Putera";
                                    }else if($jk==2){
                                        echo "Puteri";
                                    }
                                    ?></h4>
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>No HP</th>
                                    <th>Alamat</th>
                                    </thead>
                                    <tbody>
                                    <?php
                                    $anggotas="";
                                    if($jk==1){
                                        $anggotas = $func->showAnggotaPa();
                                    }else if($jk==2){
                                        $anggotas = $func->showAnggotaPi();
                                    }
                                    $no=1;
                                    while($anggota = $anggotas->fetch_assoc()){
                                        echo "
                                                <tr>
                                                    <td>".$no."</td>
                                                    <td>".$anggota['nama']."</td>
                                                    <td>".$anggota['email']."</td>
                                                    <td>".$anggota['no_hp']."</td>
                                                    <td>".$anggota['alamat']."</td>
                                                </tr>  
                                            ";
                                        $no++;
                                    }
                                    ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>


</body>

<!--   Core JS Files   -->
<script src="assets/js/jquery-1.10.2.js" type="text/javascript"></script>
<script src="assets/js/bootstrap.min.js" type="text/javascript"></script>

<!--  Checkbox, Radio & Switch Plugins -->
<script src="assets/js/bootstrap-checkbox-radio-switch.js"></script>

<!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
<script src="assets/js/light-bootstrap-dashboard.js"></script>


</html>

<?php $func->clearSession(); ?>