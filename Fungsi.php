<?php
include 'Koneksi.php';
/**
 * Created by PhpStorm.
 * User: vedo_alfarizi
 * Date: 23/06/2017
 * Time: 00.09
 */
class Fungsi
{
    private $db;

    function __construct()
    {
        $this->db = new Koneksi();
    }

    function showGaleri()
    {
        $query = $this->db->kon->query("SELECT * FROM galeri");

        return $query;
    }

    public function showSomeGaleri($start , $end)
    {
        $query = $this->db->kon->query('SELECT * FROM galeri LIMIT '.$start.','.$end.'');

        return $query;
    }

    function tambahGaleri($foto, $desk){
        $query = $this->db->kon->query("INSERT INTO galeri(`deskripsi`, `foto`) VALUES('$desk','$foto')");

        return $query;
    }

    function showOneGaleri($id){
        $query = $this->db->kon->query("SELECT * FROM galeri WHERE id_foto='$id'");

        return $query;
    }

    function hapusGaleri($id){
        $query = $this->db->kon->query("DELETE FROM galeri WHERE id_foto='$id'");

        return $query;
    }

    function editGaleri($id, $foto, $desk){
        if($foto==""){
            $query = $this->db->kon->query("UPDATE galeri SET deskripsi='$desk' WHERE id_foto='$id'");
        }else{
            $query = $this->db->kon->query("UPDATE galeri SET deskripsi='$desk', foto='$foto' WHERE id_foto='$id'");
        }

        return $query;
    }

    function showStruktur()
    {
        $query = $this->db->kon->query("SELECT * FROM pengurus WHERE id_foto_p=1");

        return $query;
    }

    function editStruktur($foto){
        $query = $this->db->kon->query("UPDATE pengurus SET foto='$foto' WHERE id_foto_p=1");

        return $query;
    }

    function showPengurus()
    {
        $query = $this->db->kon->query("SELECT * FROM pengurus WHERE id_foto_p=2");

        return $query;
    }

    function editPengurus($foto){
        $query = $this->db->kon->query("UPDATE pengurus SET foto='$foto' WHERE id_foto_p=2");

        return $query;
    }

    function tambahAnggota($nama, $email, $hp, $status, $alamat, $jk ){
        $query = $this->db->kon->query("INSERT INTO anggota (`nama`, `email`, `no_hp`, `status`, `alamat`, 
                                      `jenis_kelamin`) VALUES ('$nama', '$email', '$hp', '$status', '$alamat', '$jk')");

        return $query;
    }

    function showAnggotaPa(){
        $query = $this->db->kon->query("SELECT * FROM anggota WHERE jenis_kelamin=1 AND status_verifikasi=1");

        return $query;
    }

    public function showSomeAnggotaPa($start , $end)
    {
        $query = $this->db->kon->query('SELECT * FROM anggota WHERE jenis_kelamin=1 AND status_verifikasi=1 LIMIT '.$start.','.$end.'');

        return $query;
    }

    function showAnggotaPi(){
        $query = $this->db->kon->query("SELECT * FROM anggota WHERE jenis_kelamin=2 AND status_verifikasi=1");

        return $query;
    }

    public function showSomeAnggotaPi($start , $end)
    {
        $query = $this->db->kon->query('SELECT * FROM anggota WHERE jenis_kelamin=2 AND status_verifikasi=1 LIMIT '.$start.','.$end.'');

        return $query;
    }

    function showAnggotaAktif($jk){
        if($jk == 'pa'){
            $query = $this->db->kon->query("SELECT * FROM anggota WHERE jenis_kelamin=1 AND status_verifikasi=1 AND status=1");
        }else if($jk == 'pi'){
            $query = $this->db->kon->query("SELECT * FROM anggota WHERE jenis_kelamin=2 AND status_verifikasi=1 AND status=1");
        }
        
        return $query;
    }

    function showAlumni(){

        $query = $this->db->kon->query("SELECT * FROM anggota WHERE status_verifikasi=1 AND status=2");
    
        return $query;
    }

    public function showSomeAlumni($start , $end)
    {
        $query = $this->db->kon->query('SELECT * FROM anggota WHERE status_verifikasi=1 AND status=2 LIMIT '.$start.','.$end.'');

        return $query;
    }

    function hapusAnggota($id){
        $query = $this->db->kon->query("DELETE FROM anggota WHERE id_anggota='$id'");

        return $query;
    }

    function showVerifikasi(){
        $query = $this->db->kon->query("SELECT * FROM anggota WHERE status_verifikasi=0");

        return $query;
    }
    
    public function showSomeVerifikasi($start , $end)
    {
        $query = $this->db->kon->query('SELECT * FROM anggota WHERE status_verifikasi=0 LIMIT '.$start.','.$end.'');

        return $query;
    }

    function verifikasiAnggota($id){
        $query = $this->db->kon->query("UPDATE anggota SET status_verifikasi=1 WHERE id_anggota='$id'");

        return $query;
    }

    function showBerita(){
        $query = $this->db->kon->query("SELECT * FROM berita");

        return $query;
    }

    public function showSomeBerita($start , $end)
    {
        $query = $this->db->kon->query('SELECT * FROM berita LIMIT '.$start.','.$end.'');

        return $query;
    }

    function showOneBerita($id){
        $query = $this->db->kon->query("SELECT * FROM berita WHERE id_berita='$id'");

        return $query;
    }

    function getLastId($kolom, $tabel){
//      $lastId = $this->db->kon->query("SELECT $kolom FROM $tabel");
        $lastId = $this->db->kon->query("SELECT MAX($kolom) AS id FROM $tabel")->fetch_assoc();

        return $lastId;
    }

    function tambahBerita($foto, $judul, $isi){
        $query = $this->db->kon->query("INSERT INTO berita(`judul`, `isi`, `foto`) VALUES('$judul','$isi','$foto')");

        return $query;
    }

    function editBerita($id, $foto, $judul, $isi){
        if($foto==""){
            $query = $this->db->kon->query("UPDATE berita SET judul='$judul', isi='$isi' WHERE id_berita='$id'");
        }else{
            $query = $this->db->kon->query("UPDATE berita SET judul='$judul', isi='$isi', foto='$foto' WHERE id_berita='$id'");
        }

        return $query;
    }

    function hapusBerita($id){
        $query = $this->db->kon->query("DELETE FROM berita WHERE id_berita='$id'");

        return $query;
    }

    public function getJenisKelamin($jk)
    {
        if($jk == 1){
            $jenis = 'Putera';
        }else if($jk == 2){
            $jenis = 'Puteri';
        }

        return $jenis;
    }

    public function auth()
    {
        if(!isset($_SESSION['uname'])){
            header('location:login.php');
        }
    }

    public function cekSession()
    {
        if(!isset($_SESSION)){
            session_start();
        }

        if(!isset($_SESSION['pesan'])){
            $_SESSION['pesan']='';
        }
    }

    public function clearSession()
    {
        unset($_SESSION['pesan']);
    }

    public function cekUser($username)
    {
        $cek = $this->db->kon->query('SELECT COUNT(id) AS jumlah FROM admin WHERE username=\''.$username.'\'');
        $hasil = $cek->fetch_assoc();

        return $hasil['jumlah'];
    }

    public function login($uname, $pass)
    {   
        $cek = $this->db->kon->query('SELECT * FROM admin WHERE username=\''.$uname.'\' AND password=\''.$pass.'\'');

        return $cek;
    }
}