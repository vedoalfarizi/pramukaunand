-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 27 Jul 2017 pada 23.10
-- Versi Server: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `pramuka`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(191) NOT NULL,
  `password` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Struktur dari tabel `anggota`
--

CREATE TABLE `anggota` (
  `id_anggota` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `no_hp` varchar(20) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `jenis_kelamin` tinyint(1) NOT NULL,
  `status_verifikasi` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `anggota`
--

INSERT INTO `anggota` (`id_anggota`, `nama`, `email`, `no_hp`, `status`, `alamat`, `jenis_kelamin`, `status_verifikasi`) VALUES
(2, 'vedo', 'vedoalfarizi@student.unand.ac.id', '086236123123', 1, 'payakumbuah', 1, 1),
(4, 'test2', 'test@gmail.com', '089520568295', 1, 'testtest', 1, 1),
(5, 'test3', 'test@gmail.com', '086236123123', 1, 'ooooooo', 2, 1),
(6, 'testanggota', 'testlagi@gmail.com', '081234567812', 1, 'padang panjang', 1, 1),
(10, 'test', 'test@gmail.com', '08123456789', 1, 'testtest', 1, 1),
(11, 'alumni daftar', 'sdasda@asa.asd', '089520568295', 2, 'alumni putri', 2, 1),
(12, 'test9', 'test9@gmail.com', '086623834333', 1, 'apa?', 2, 1),
(14, 'test11', 'test11@gmail.com', '08736574833', 2, 'riau', 1, 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `berita`
--

CREATE TABLE `berita` (
  `id_berita` int(11) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `isi` text NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `berita`
--

INSERT INTO `berita` (`id_berita`, `judul`, `isi`, `foto`) VALUES
(6, 'apa yaaa', 'hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm hmmmmm ', 'berita_2.jpg'),
(7, 'coba dg id baru', 'berita hjskdhak fasfjkl,df sdg', 'berita_1.jpg'),
(8, 'skjhdkajshdkjashd', 'lkajsfklsdfhkljsdf skdjhf skjdhf lkajsfklsdfhkljsdf skdjhf skjdhf lkajsfklsdfhkljsdf skdjhf skjdhf lkajsfklsdfhkljsdf skdjhf skjdhf lkajsfklsdfhkljsdf skdjhf skjdhf', 'berita_8.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `galeri`
--

CREATE TABLE `galeri` (
  `id_foto` int(11) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `galeri`
--

INSERT INTO `galeri` (`id_foto`, `deskripsi`, `foto`) VALUES
(2, 'tambah foto pertama', 'galeri_2.jpg'),
(3, 'kedua', 'galeri_3.jpg'),
(4, 'test', 'galeri_4.jpg'),
(6, 'hmmm', 'galeri_6.jpg'),
(7, 'klo ini', 'galeri_7.jpg'),
(8, 'aneh juga', 'galeri_8.jpg'),
(9, 'coba', 'galeri_9.jpg');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengurus`
--

CREATE TABLE `pengurus` (
  `id_foto_p` int(11) NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `pengurus`
--

INSERT INTO `pengurus` (`id_foto_p`, `foto`) VALUES
(1, 'struktur_3.jpg'),
(2, 'pengurus.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `anggota`
--
ALTER TABLE `anggota`
  ADD PRIMARY KEY (`id_anggota`);

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id_berita`);

--
-- Indexes for table `galeri`
--
ALTER TABLE `galeri`
  ADD PRIMARY KEY (`id_foto`);

--
-- Indexes for table `pengurus`
--
ALTER TABLE `pengurus`
  ADD PRIMARY KEY (`id_foto_p`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `anggota`
--
ALTER TABLE `anggota`
  MODIFY `id_anggota` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id_berita` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `galeri`
--
ALTER TABLE `galeri`
  MODIFY `id_foto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `pengurus`
--
ALTER TABLE `pengurus`
  MODIFY `id_foto_p` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
