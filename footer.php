<footer>
    <div class="container">
        <div class="row">
            <div class="col-12"></div>
            <div>
                <div class="pull-right">
                    Pramuka <br>
                    Universitas Andalas <br>
                    Gedung PKM lantai 2 sayap kanan <br>
                    <a href="https://www.facebook.com/racanaswarnadwipa" target="_blank"><img src="assets/images/social/fb.png" alt="" width="5%"></a>
                    <a href="https://twitter.com/pramukaunand" target="_blank"><img src="assets/images/social/tw.png" alt="" width="5%"></a>
                    <a href="https://www.instagram.com/racana_swarnadwipa/" target="_blank"><img src="assets/images/social/ig.png" alt="" width="5%"></a>
                    <a href="https://www.youtube.com/channel/UC5-PVKRoWhOgirG2PEUMyBQ" target="_blank"><img src="assets/images/social/yt.png" alt="" width="5%"></a>
                    <a href="https://plus.google.com/102956898629190355063" target="_blank"><img src="assets/images/social/gm.png" alt="" width="5%"></a>
                </div>
                <div class="pull-right" style="margin-right: 1%">
                    <img class="img-responsive" src="assets/images/logo-racana.png" style="max-width: 110px;">
                </div>
            </div>
        </div>
        <div class="row" style="text-align: center">
            <br> Copyright &copy 2017 Pramuka Unand
        </div>
    </div>
</footer>